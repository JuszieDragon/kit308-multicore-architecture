// SIMD versions of (some of the) helper functions from primitives.h

#ifndef __PRIMITIVES_SIMD_H
#define __PRIMITIVES_SIMD_H

#include <immintrin.h>

// a bunch of operators to replace nasty instrinsics
__forceinline __m256 operator - (const __m256 x, const __m256 y) { return _mm256_sub_ps(x, y); }
__forceinline __m256 operator + (const __m256 x, const __m256 y) { return _mm256_add_ps(x, y); }
__forceinline __m256 operator * (const __m256 x, const __m256 y) { return _mm256_mul_ps(x, y); }
__forceinline __m256 operator / (const __m256 x, const __m256 y) { return _mm256_div_ps(x, y); }
__forceinline __m256 operator < (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_LT_OQ); }
__forceinline __m256 operator > (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_GT_OQ); }
__forceinline __m256 operator <= (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_LE_OQ); }
__forceinline __m256 operator >= (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_GE_OQ); }
__forceinline __m256 operator & (const __m256 x, const __m256 y) { return _mm256_and_ps(x, y); }
__forceinline __m256 operator | (const __m256 x, const __m256 y) { return _mm256_or_ps(x, y); }
__forceinline __m256 operator == (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_EQ_OQ); }
__forceinline __m256 operator != (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_NEQ_OQ); }

__forceinline __m256i operator & (const __m256i x, const __m256i y) { return _mm256_and_si256(x, y); }
__forceinline __m256i operator | (const __m256i x, const __m256i y) { return _mm256_or_si256(x, y); }
__forceinline __m256i operator + (const __m256i x, const __m256i y) { return _mm256_add_epi32(x, y); }

// Represent 8 vectors in one struct
struct Vector8
{
	__m256 xs, ys, zs;

	__forceinline Vector8(float x, float y, float z)
	{
		xs = _mm256_set1_ps(x);
		ys = _mm256_set1_ps(y);
		zs = _mm256_set1_ps(z);
	}

	__forceinline Vector8(__m256 xsIn, __m256 ysIn, __m256 zsIn)
	{
		xs = xsIn;
		ys = ysIn;
		zs = zsIn;
	}

	__forceinline Vector8()
	{
		xs = ys = zs = _mm256_setzero_ps();
	}
};


// helper operators / functions for Vector8s
__forceinline Vector8 operator - (const Vector8& v1, const Vector8& v2) { return { v1.xs - v2.xs, v1.ys - v2.ys, v1.zs - v2.zs }; }
__forceinline Vector8 operator + (const Vector8& v1, const Vector8& v2) { return { v1.xs + v2.xs, v1.ys + v2.ys, v1.zs + v2.zs }; }
__forceinline Vector8 operator * (const Vector8& v1, const Vector8& v2) { return { v1.xs * v2.xs, v1.ys * v2.ys, v1.zs * v2.zs }; }
__forceinline Vector8 operator / (const Vector8& v1, const Vector8& v2) { return { v1.xs / v2.xs, v1.ys / v2.ys, v1.zs / v2.zs }; }

__forceinline Vector8 cross(const Vector8& v1, const Vector8& v2)
{
	return { v1.ys * v2.zs - v1.zs * v2.ys, v1.zs * v2.xs - v1.xs * v2.zs, v1.xs * v2.ys - v1.ys * v2.xs };
}

__forceinline __m256 dot(const Vector8& v1, const Vector8& v2)
{
	return v1.xs * v2.xs + v1.ys * v2.ys + v1.zs * v2.zs;
}

inline Vector8 operator * (const Vector8& v, __m256 c)
{
	Vector8 v2 = { v.xs * c, v.ys * c, v.zs * c };
	return v2;
}

// Represent 8 vectors in one struct
struct Colour8
{
	__m256 reds, greens, blues;

	__forceinline Colour8(float x, float y, float z)
	{
		reds = _mm256_set1_ps(x);
		greens = _mm256_set1_ps(y);
		blues = _mm256_set1_ps(z);
	}

	__forceinline Colour8(__m256 redsIn, __m256 greensIn, __m256 bluesIn)
	{
		reds = redsIn;
		greens = greensIn;
		blues = bluesIn;
	}

	__forceinline Colour8()
	{
		reds = greens = blues = _mm256_setzero_ps();
	}

	// colour += colour
	inline Colour8& operator += (const Colour8& c2)
	{
		this->reds = this->reds + c2.reds;
		this->greens = this->greens + c2.greens;
		this->blues = this->blues + c2.blues;
		return *this;
	}
};


// helper operators / functions for Vector8s
__forceinline Colour8 operator - (const Colour8& v1, const Colour8& v2) { return { v1.reds - v2.reds, v1.greens - v2.greens, v1.blues - v2.blues }; }
__forceinline Colour8 operator + (const Colour8& v1, const Colour8& v2) { return { v1.reds + v2.reds, v1.greens + v2.greens, v1.blues + v2.blues }; }
__forceinline Colour8 operator * (const Colour8& v1, const Colour8& v2) { return { v1.reds * v2.reds, v1.greens * v2.greens, v1.blues * v2.blues }; }
__forceinline Colour8 operator / (const Colour8& v1, const Colour8& v2) { return { v1.reds / v2.reds, v1.greens / v2.greens, v1.blues / v2.blues }; }

struct Point8
{
	__m256 xs, ys, zs;

	__forceinline Point8(Point startsIn)
	{
		this->xs = _mm256_set1_ps(startsIn.x);
		this->ys = _mm256_set1_ps(startsIn.y);
		this->zs = _mm256_set1_ps(startsIn.z);
	}

	__forceinline Point8(__m256 xIn, __m256 yIn, __m256 zIn)
	{
		this->xs = xIn;
		this->ys = yIn;
		this->zs = zIn;
	}

	__forceinline Point8()
	{
		xs = ys = zs = _mm256_setzero_ps();
	}
};

inline Vector8 operator - (const Point8& p1, const Point8& p2)
{
	Vector8 v = { p1.xs - p2.xs, p1.ys - p2.ys, p1.zs - p2.zs };
	return v;
}

struct Ray8
{
	Point8 starts;
	Vector8 dirs;

	__forceinline Ray8(Point8 startsIn, Vector8 dirIn)
	{
		this->starts = startsIn;
		this->dirs = dirIn;
	}
};



// select helper functions
__forceinline __m256 select(__m256 cond, __m256 ifTrue, __m256 ifFalse)
{
	return _mm256_or_ps(_mm256_and_ps(cond, ifTrue), _mm256_andnot_ps(cond, ifFalse));
}

__forceinline __m256i select(__m256i cond, __m256i ifTrue, __m256i ifFalse)
{
	return _mm256_or_si256(_mm256_and_si256(cond, ifTrue), _mm256_andnot_si256(cond, ifFalse));
}


#endif

