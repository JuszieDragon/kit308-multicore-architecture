/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#define TARGET_WINDOWS

#pragma warning(disable: 4996)
#include <stdio.h>
#include "Timer.h"
#include "Primitives.h"
#include "Scene.h"
#include "Lighting.h"
#include "Intersection.h"
#include "ImageIO.h"
#include <Windows.h>

unsigned int buffer[MAX_WIDTH * MAX_HEIGHT];
// a global variable to store the number of samples rendered
LONG samplesRendered = 0;

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabsf(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrtf(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrtf(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}


// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(const Scene* scene, Ray viewRay)
{
	Colour output(0.0f, 0.0f, 0.0f); 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

																	// loop until reached maximum ray cast limit (unless loop is broken out of)
	for (int level = 0; level < MAX_RAYS_CAST; ++level)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (!objectIntersection(scene, &viewRay, &intersect)) break;

		// calculate response to collision: ie. get normal at point of collision and material of object
		calculateIntersectionResponse(scene, &viewRay, &intersect);

		// apply the diffuse and specular lighting 
		if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

		// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
		if (intersect.material->reflection)
		{
			viewRay = calculateReflection(&viewRay, &intersect);
			coef *= intersect.material->reflection;
		}
		else if (intersect.material->refraction)
		{
			viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
			coef *= intersect.material->refraction;
		}
		else
		{
			// if no reflection or refraction, then finish looping (cast no more rays)
			return output;
		}
	}

	// if the calculation coefficient is non-zero, read from the environment map
	if (coef > 0.0f)
	{
		Material& currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

		output += coef * currentMaterial.diffuse;
	}

	return output;
}


// render scene at given width and height and anti-aliasing level
int render(Scene* scene, const int threadID, const int threads, const int width, const int height, const int aaLevel, bool testMode, bool colourise, unsigned int* out, LONG* previousBlock, int blockSize)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tanf(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	// constants to account for the zero point being in the middle of the image
	const int heightOffset = height / 2;
	const int widthOffset = width / 2;

	// calculate and store how many blocks are needed for the image width
	unsigned int numBlocksX = width / blockSize;

	// increases the number of x blocks by one if the image isn't cleanly divisible by the image width so that the smaller blocks are still done
	if (width % blockSize != 0)
	{
		numBlocksX++;
	}

	// calculate and store how many blocks are needed for the image height
	unsigned int numBlocksY = height / blockSize;
	
	// increases the number of y blocks by one if the image isn't cleanly divisible by the image height so that the smaller blocks are still done
	if (height % blockSize != 0)
	{
		numBlocksY++;
	}

	int currentBlockIndex;
	int samples = 0;

	while ((currentBlockIndex = InterlockedIncrement(previousBlock)) < (numBlocksX * numBlocksY))
	{
		// calculate and store in a variable the current block x-coordinate
		int bx = (currentBlockIndex % numBlocksX);
		// calculate and store in a variable the current block y-coordinate
		int by = (currentBlockIndex / numBlocksX);

		for (int iy = (by * blockSize) - heightOffset; iy < (((by + 1) * blockSize) - heightOffset) && iy < heightOffset; iy += 1)
		{
			for (int ix = (bx * blockSize) - widthOffset; ix < (((bx + 1) * blockSize) - widthOffset) && ix < widthOffset; ix += 1)
			{
				Colour output;

				// if colourise is set tint each threads work
				if (colourise)
				{
					float colourValue = ((float)(threadID) / (float)threads);

					output.red = colourValue * (threadID % 2);
					output.blue = colourValue * (threadID % 3);
					output.green = colourValue * (threadID % 5);
				}
				else
				{
					output.red = 0.0f;
					output.blue = 0.0f;
					output.green = 0.0f;
				}

				// calculate multiple samples for each pixel
				const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

				// loop through all sub-locations within the pixel
				for (float fragmentx = float(ix); fragmentx < ix + 1.0f; fragmentx += sampleStep)
				{
					for (float fragmenty = float(iy); fragmenty < iy + 1.0f; fragmenty += sampleStep)
					{
						// direction of default forward facing ray
						Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };

						// rotated direction of ray
						Vector rotatedDir = {
							dir.x * cosf(scene->cameraRotation) - dir.z * sinf(scene->cameraRotation),
							dir.y,
							dir.x * sinf(scene->cameraRotation) + dir.z * cosf(scene->cameraRotation) };

						// view ray starting from camera position and heading in rotated (normalised) direction
						Ray viewRay = { scene->cameraPosition, normalise(rotatedDir) };

						// follow ray and add proportional of the result to the final pixel colour
						output += sampleRatio * traceRay(scene, viewRay);

						// count this sample
						samples++;
					}
				}

				if (!testMode)
				{
					// store saturated final colour value in image buffer
					out[(iy + heightOffset) * width + (ix + widthOffset)] = output.convertToPixel(scene->exposure);
				}
				else
				{
					// give each thread a unique shade of gray based on threadID
					float colourValue = (float)(threadID + 1) / (float)threads;
					// store white in image buffer (with multiple threads this should store a grey based on the thread number)
					out[(iy + heightOffset) * width + (ix + widthOffset)] = Colour(colourValue, colourValue, colourValue).convertToPixel();
				}
			}
		}
	}

	InterlockedAdd(&samplesRendered, samples);

	return samplesRendered;
}

// ThreadData structure to hold all the parameters necessary for running the mandelbrot function
struct ThreadData
{
	Scene* scene;
	int threadID;
	int threads;
	int aaLevel;
	int width;
	int height;
	bool testMode;
	bool colourise;
	unsigned int* out;
	LONG* previousBlock;
	int blockSize;
};

DWORD __stdcall renderThreadStart(LPVOID threadData)
{
	// cast the pointer to void (i.e. an untyped pointer) into something we can use
	ThreadData* data = (ThreadData*)threadData;

	// pass in parameters
	render(data->scene, data->threadID, data->threads, data->width, data->height, data->aaLevel, data->testMode, data->colourise, data->out, data->previousBlock, data->blockSize);

	ExitThread(NULL);
}

// read command line arguments, render, and write out BMP file
int main(int argc, char* argv[])
{
	int width = 1024;
	int height = 1024;
	int samples = 1;

	// rendering options
	int times = 1;
	bool testMode = false;
	unsigned int threads = 12;
	bool colourise = false;
	unsigned int blockSize = 8;

	// default input / output filenames
	const char* inputFilename = "Scenes/cornell.txt";

	char outputFilenameBuffer[1000];
	char* outputFilename = outputFilenameBuffer;

	// do stuff with command line args
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			width = atoi(argv[++i]);
			height = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-samples") == 0)
		{
			samples = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-input") == 0)
		{
			inputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-output") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-runs") == 0)
		{
			times = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-threads") == 0)
		{
			threads = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-colourise") == 0)
		{
			colourise = true;
		}
		else if (strcmp(argv[i], "-blockSize") == 0)
		{
			blockSize = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-testMode") == 0)
		{
			testMode = true;
		}
		else
		{
			fprintf(stderr, "unknown argument: %s\n", argv[i]);
		}
	}

	// nasty (and fragile) kludge to make an ok-ish default output filename (can be overriden with "-output" command line option)
	sprintf(outputFilenameBuffer, "Outputs/%s_%dx%dx%d_%s.bmp", (strrchr(inputFilename, '/') + 1), width, height, samples, (strrchr(argv[0], '\\') + 1));

	// read scene file
	Scene scene;
	if (!init(inputFilename, scene))
	{
		fprintf(stderr, "Failure when reading the Scene file.\n");
		return -1;
	}

	// total time taken to render all runs (used to calculate average)
	int totalTime = 0;

	for (int i = 0; i < times; i++)
	{
		Timer timer;	// create timer

		// dynamically sized storage for Thread handles and initialisation data
		HANDLE* threadHandles = new HANDLE[threads];
		ThreadData* threadData = new ThreadData[threads];

		// declare a variable to act as shared memory for the threads
		LONG previousBlock = -1;
		// reset the global samples varibable for each run
		samplesRendered = 0;

		// create all the threads with sensible initial values
		for (unsigned int i = 0; i < threads; i++) 
		{
			threadData[i].scene = &scene;
			threadData[i].threadID = i;
			threadData[i].threads = threads;
			threadData[i].aaLevel = samples;
			threadData[i].width = width;
			threadData[i].height = height;
			threadData[i].previousBlock = &previousBlock;
			threadData[i].blockSize = blockSize;
			threadData[i].testMode = testMode;
			threadData[i].colourise = colourise;
			threadData[i].out = buffer;

			threadHandles[i] = CreateThread(NULL, 0, renderThreadStart, (void*)&threadData[i], 0, NULL);
		}

		// wait for everything to finish
		for (unsigned int i = 0; i < threads; i++) 
		{
			WaitForSingleObject(threadHandles[i], INFINITE);
		}

		// release dynamic memory
		delete[] threadHandles;
		delete[] threadData;

		timer.end();														// record end time
		totalTime += timer.getMilliseconds();								// record total time taken
	}

	// output timing information (times run and average)
	printf("rendered %d samples, average time taken (%d run(s)): %ums\n", samplesRendered, times, totalTime / times);

	// output BMP file
	write_bmp(outputFilename, buffer, width, height, width);
}
