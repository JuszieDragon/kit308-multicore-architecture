/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#define TARGET_WINDOWS

#pragma warning(disable: 4996)
#include <stdio.h>
#include "Timer.h"
#include "Primitives.h"
#include "Scene.h"
#include "Lighting.h"
#include "Intersection.h"
#include "ImageIO.h"
#include <Windows.h>

unsigned int buffer[MAX_WIDTH * MAX_HEIGHT];
// a global variable to store the number of samples rendered
LONG samplesRendered = 0;

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabsf(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrtf(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrtf(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}


// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(const Scene* scene, Ray viewRay)
{
	Colour output(0.0f, 0.0f, 0.0f); 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

																	// loop until reached maximum ray cast limit (unless loop is broken out of)
	for (int level = 0; level < MAX_RAYS_CAST; ++level)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (!objectIntersection(scene, &viewRay, &intersect)) break;

		// calculate response to collision: ie. get normal at point of collision and material of object
		calculateIntersectionResponse(scene, &viewRay, &intersect);

		// apply the diffuse and specular lighting 
		if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

		// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
		if (intersect.material->reflection)
		{
			viewRay = calculateReflection(&viewRay, &intersect);
			coef *= intersect.material->reflection;
		}
		else if (intersect.material->refraction)
		{
			viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
			coef *= intersect.material->refraction;
		}
		else
		{
			// if no reflection or refraction, then finish looping (cast no more rays)
			return output;
		}
	}

	// if the calculation coefficient is non-zero, read from the environment map
	if (coef > 0.0f)
	{
		Material& currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

		output += coef * currentMaterial.diffuse;
	}

	return output;
}


// render scene at given width and height and anti-aliasing level
int render(Scene* scene, const int width, const int height, const int aaLevel, int startY, int endY, bool testMode, bool colourise, int threadID, int threads, unsigned int* out)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tanf(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	// loop through all the pixels
	for (int y = startY; y < endY; ++y)
	{
		for (int x = -width / 2; x < width / 2; ++x)
		{
			Colour output;

			// if colourisse is set tint each threads work
			if (colourise)
			{
				float colourValue = ((float)(threadID) / (float)threads);

				output.red = colourValue * (threadID % 2);
				output.blue = colourValue * (threadID % 3);
				output.green = colourValue * (threadID % 5);
			}
			else
			{
				output.red = 0.0f;
				output.blue = 0.0f;
				output.green = 0.0f;
			}

			// calculate multiple samples for each pixel
			const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

			// loop through all sub-locations within the pixel
			for (float fragmentx = float(x); fragmentx < x + 1.0f; fragmentx += sampleStep)
			{
				for (float fragmenty = float(y); fragmenty < y + 1.0f; fragmenty += sampleStep)
				{
					// direction of default forward facing ray
					Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };
					//printf("CentreValueThingy: %f\n", centreY + fragmenty * dirStepSize);

					// rotated direction of ray
					Vector rotatedDir = {
						dir.x * cosf(scene->cameraRotation) - dir.z * sinf(scene->cameraRotation),
						dir.y,
						dir.x * sinf(scene->cameraRotation) + dir.z * cosf(scene->cameraRotation) };

					// view ray starting from camera position and heading in rotated (normalised) direction
					Ray viewRay = { scene->cameraPosition, normalise(rotatedDir) };

					// follow ray and add proportional of the result to the final pixel colour
					output += sampleRatio * traceRay(scene, viewRay);

					// count this sample
					InterlockedIncrement(&samplesRendered);
				}
			}

			if (!testMode)
			{
				// store saturated final colour value in image buffer
				*out++ = output.convertToPixel(scene->exposure);
			}
			else
			{

				// give each thread a unique shade of gray based on threadID
				float colourValue = (float)(threadID + 1) / (float)threads;
				//store white in image buffer (with multiple threads this should store a grey based on the thread number)
				*out++ = Colour(colourValue, colourValue, colourValue).convertToPixel();
			}
		}
	}

	return samplesRendered;
}

// declare a ThreadData (or similarly named) structure to hold all the parameters necessary for running the mandelbrot function
struct ThreadData {
	Scene* scene;
	int width;
	int height;
	int aaLevel;
	int startY;
	int endY;
	bool testMode;
	bool colourise;
	int threadID;
	int threads;
	unsigned int* out;
};

// declare a THREAD_START_ROUTINE function that casts its argument, calls the mandelbrot function, and then exits gracefully
DWORD renderThreadStart(LPVOID threadData)
{
	ThreadData* data = (ThreadData*)threadData;

	render(data->scene, data->width, data->height, data->aaLevel, data->startY, data-> endY, data->testMode, data->colourise, data->threadID, data->threads, data->out);

	ExitThread(NULL);
}

// read command line arguments, render, and write out BMP file
int main(int argc, char* argv[])
{
	int width = 1024;
	int height = 1024;
	int samples = 1;

	// rendering options
	int times = 10;
	bool testMode = false;
	int threads = 8;
	bool colourise = false;
	int blockSize = -1;		// currently unused

	// default input / output filenames
	const char* inputFilename = "Scenes/cornell.txt";

	char outputFilenameBuffer[1000];
	char* outputFilename = outputFilenameBuffer;

	// do stuff with command line args
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			width = atoi(argv[++i]);
			height = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-samples") == 0)
		{
			samples = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-input") == 0)
		{
			inputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-output") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-runs") == 0)
		{
			times = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-threads") == 0)
		{
			threads = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-colourise") == 0)
		{
			colourise = true;
		}
		else if (strcmp(argv[i], "-blockSize") == 0)
		{
			blockSize = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-testMode") == 0)
		{
			testMode = true;
		}
		else
		{
			fprintf(stderr, "unknown argument: %s\n", argv[i]);
		}
	}

	// nasty (and fragile) kludge to make an ok-ish default output filename (can be overriden with "-output" command line option)
	sprintf(outputFilenameBuffer, "Outputs/%s_%dx%dx%d_%s.bmp", (strrchr(inputFilename, '/') + 1), width, height, samples, (strrchr(argv[0], '\\') + 1));

	// read scene file
	Scene scene;
	if (!init(inputFilename, scene))
	{
		fprintf(stderr, "Failure when reading the Scene file.\n");
		return -1;
	}

	// total time taken to render all runs (used to calculate average)
	int totalTime = 0;

	for (int i = 0; i < times; i++)
	{
		Timer timer;														// create timer

		// dynamically sized storage for Thread handles and initialisation data
		HANDLE* threadHandles = new HANDLE[threads];
		ThreadData* threadData = new ThreadData[threads];

		// these variables store calculate how lines have been lost due to int "rounding"
		int chunkSize = height / threads;
		int actualSize = chunkSize * threads;
		int missingLines = height - actualSize;

		// used to clean up the offsets a bit
		int negHalfHeight = (-height / 2);
		
		for (int i = 0; i < threads; i++)
		{
			threadData[i].scene = &scene;
			threadData[i].testMode = testMode;
			threadData[i].width = width;
			threadData[i].height = height / threads;
			threadData[i].aaLevel = samples;
			threadData[i].testMode = testMode;
			threadData[i].colourise = colourise;
			threadData[i].threadID = i;
			threadData[i].threads = threads;

			// store how many pixels have been calculated already
			long long pixelsDone = (long long)chunkSize * (long long)width * (long long)i;

			// for every missing line give a thread an extra line and account for the offset
			if (i < missingLines)
			{
				threadData[i].startY = (negHalfHeight + chunkSize * i) + i;
				threadData[i].endY = (negHalfHeight + chunkSize * (i + 1)) + (i + 1);
				threadData[i].out = buffer + pixelsDone + ((long long)i * (long long)width);
			}
			else
			{
				threadData[i].startY = (negHalfHeight + chunkSize * i) + missingLines;
				threadData[i].endY = (negHalfHeight + chunkSize * (i + 1)) + missingLines;
				threadData[i].out = buffer + pixelsDone + ((long long)missingLines * (long long)width);
			}

			threadHandles[i] = CreateThread(NULL, 0, renderThreadStart, (void*)&threadData[i], 0, NULL);
		}

		// wait for everything to finish
		for (unsigned int i = 0; i < threads; i++)
		{
			WaitForSingleObject(threadHandles[i], INFINITE);
		}

		// delete the dynamically declared arrays
		delete[] threadHandles;
		delete[] threadData;

		//samplesRendered = render(&scene, width, height, samples, testMode);	// raytrace scene

		timer.end();														// record end time
		totalTime += timer.getMilliseconds();								// record total time taken
	}

	// output timing information (times run and average)
	printf("rendered %d samples, average time taken (%d run(s)): %ums\n", samplesRendered / times, times, totalTime / times);

	// output BMP file
	write_bmp(outputFilename, buffer, width, height, width);
}
