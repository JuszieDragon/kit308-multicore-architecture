/////////////////////////////////////////
// Sixth version of the scene file format
// 
// - It allows you to add comments like this one
// - Syntax itself is hopefully self explanatory
// - Name of the objects and attributes are defined inside the executable

///////////////////////////////////////
//    Global scene and viewpoint     //
/////////////////////////////////////// 

Scene 
{
	// make sure the version and the executable match !
	Version.Major = 1;
	Version.Minor = 5;

	Camera.Position = 0.0, 0.0, 1000.0;
	Camera.Rotation = 0.0;
	Camera.FieldOfView = 90.0;

	// Image Exposure
	Exposure = -2.5;
	
	Skybox.Material.Id = 0;

	// Count the objects in the scene
	NumberOfMaterials = 5;
	NumberOfSpheres = 40;
	NumberOfLights = 1; 
	NumberOfBoxes = 5;
}

///////////////////////////////////////
//         List of materials         //
/////////////////////////////////////// 

Material0
{
	Type = gouraud;
	Diffuse = 0.75, 0.75, 0.75;
	Specular = 1.2, 1.2, 1.2;  
	Power = 60;
	Reflection = 0.05;
}
Material1
{
	Type = gouraud;
	Diffuse = 0.75, 0.25, 0.25;
	Specular = 1.2, 1.2, 1.2;  
	Power = 60;
	Reflection = 0.05;
}
Material2
{
	Type = gouraud;
	Diffuse = 0.25, 0.25, 0.75;
	Specular = 1.2, 1.2, 1.2;  
	Power = 60;
}
Material3
{
	Type = gouraud;
	Reflection = 1.0;
	Specular = 1.5, 1.5, 1.5;  
	Power = 30;
}
Material4
{
	Type = gouraud;
	Refraction = 1.0;
	Density = 2.0;
	Specular = 1.5, 1.5, 1.5;  
	Power = 30;
}

///////////////////////////////////////
//         List of boxes             //
/////////////////////////////////////// 

Box0
{
    Point1 = -401, -401, 0;
    Point2 = 401, -400, 2900;
	Material.Id = 0;
}
Box1
{
    Point1 = -401, 400, 0;
    Point2 = 401, 401, 2900;
	Material.Id = 0;
}
Box2
{
    Point1 = -401, -401, 0;
    Point2 = -400, 401, 2900;
	Material.Id = 1;
}
Box3
{
    Point1 = 400, -401, 0;
    Point2 = 401, 401, 2900;
	Material.Id = 2;
}
Box4
{
    Point1 = -401, -401, 800;
    Point2 = 401, 401, 801;
	Material.Id = 0;
}

///////////////////////////////////////
//         List of spheres           //
/////////////////////////////////////// 
Sphere0
{
    Center = 0.0, 270.0, 2800;
    Size = 70.0;
    Material.Id = 4;
}
Sphere1
{
    Center = 83.4, 256.8, 2730;
    Size = 70.0;
    Material.Id = 4;
}
Sphere2
{
    Center = 158.7, 218.4, 2660;
    Size = 70.0;
    Material.Id = 4;
}
Sphere3
{
    Center = 218.4, 158.7, 2590;
    Size = 70.0;
    Material.Id = 4;
}
Sphere4
{
    Center = 256.8, 83.4, 2520;
    Size = 70.0;
    Material.Id = 4;
}
Sphere5
{
    Center = 270.0, 0.0, 2450;
    Size = 70.0;
    Material.Id = 4;
}
Sphere6
{
    Center = 256.8, -83.4, 2380;
    Size = 70.0;
    Material.Id = 4;
}
Sphere7
{
    Center = 218.4, -158.7, 2310;
    Size = 70.0;
    Material.Id = 4;
}
Sphere8
{
    Center = 158.7, -218.4, 2240;
    Size = 70.0;
    Material.Id = 4;
}
Sphere9
{
    Center = 83.4, -256.8, 2170;
    Size = 70.0;
    Material.Id = 4;
}
Sphere10
{
    Center = 0.0, -270.0, 2100;
    Size = 70.0;
    Material.Id = 4;
}
Sphere11
{
    Center = -83.4, -256.8, 2030;
    Size = 70.0;
    Material.Id = 4;
}
Sphere12
{
    Center = -158.7, -218.4, 1960;
    Size = 70.0;
    Material.Id = 4;
}
Sphere13
{
    Center = -218.4, -158.7, 1890;
    Size = 70.0;
    Material.Id = 4;
}
Sphere14
{
    Center = -256.8, -83.4, 1820;
    Size = 70.0;
    Material.Id = 4;
}
Sphere15
{
    Center = -270.0, -0.0, 1750;
    Size = 70.0;
    Material.Id = 4;
}
Sphere16
{
    Center = -256.8, 83.4, 1680;
    Size = 70.0;
    Material.Id = 4;
}
Sphere17
{
    Center = -218.4, 158.7, 1610;
    Size = 70.0;
    Material.Id = 4;
}
Sphere18
{
    Center = -158.7, 218.4, 1540;
    Size = 70.0;
    Material.Id = 4;
}
Sphere19
{
    Center = -83.4, 256.8, 1470;
    Size = 70.0;
    Material.Id = 4;
}
Sphere20
{
    Center = -0.0, 270.0, 1400;
    Size = 70.0;
    Material.Id = 4;
}
Sphere21
{
    Center = 83.4, 256.8, 1330;
    Size = 70.0;
    Material.Id = 4;
}
Sphere22
{
    Center = 158.7, 218.4, 1260;
    Size = 70.0;
    Material.Id = 4;
}
Sphere23
{
    Center = 218.4, 158.7, 1190;
    Size = 70.0;
    Material.Id = 4;
}
Sphere24
{
    Center = 256.8, 83.4, 1120;
    Size = 70.0;
    Material.Id = 4;
}
Sphere25
{
    Center = 270.0, 0.0, 1050;
    Size = 70.0;
    Material.Id = 4;
}
Sphere26
{
    Center = 256.8, -83.4, 980;
    Size = 70.0;
    Material.Id = 4;
}
Sphere27
{
    Center = 218.4, -158.7, 910;
    Size = 70.0;
    Material.Id = 4;
}
Sphere28
{
    Center = 158.7, -218.4, 840;
    Size = 70.0;
    Material.Id = 4;
}
Sphere29
{
    Center = 83.4, -256.8, 770;
    Size = 70.0;
    Material.Id = 4;
}
Sphere30
{
    Center = 0.0, -270.0, 700;
    Size = 70.0;
    Material.Id = 4;
}
Sphere31
{
    Center = -83.4, -256.8, 630;
    Size = 70.0;
    Material.Id = 4;
}
Sphere32
{
    Center = -158.7, -218.4, 560;
    Size = 70.0;
    Material.Id = 4;
}
Sphere33
{
    Center = -218.4, -158.7, 490;
    Size = 70.0;
    Material.Id = 4;
}
Sphere34
{
    Center = -256.8, -83.4, 420;
    Size = 70.0;
    Material.Id = 4;
}
Sphere35
{
    Center = -270.0, -0.0, 350;
    Size = 70.0;
    Material.Id = 4;
}
Sphere36
{
    Center = -256.8, 83.4, 280;
    Size = 70.0;
    Material.Id = 4;
}
Sphere37
{
    Center = -218.4, 158.7, 210;
    Size = 70.0;
    Material.Id = 4;
}
Sphere38
{
    Center = -158.7, 218.4, 140;
    Size = 70.0;
    Material.Id = 4;
}
Sphere39
{
    Center = -83.4, 256.8, 70;
    Size = 70.0;
    Material.Id = 4;
}


///////////////////////////////////////
//         List of lights            //
/////////////////////////////////////// 

Light0
{
  Position = 50.0, 100.0, 1100.0;
  Intensity = 0.5, 0.5, 0.5;
}