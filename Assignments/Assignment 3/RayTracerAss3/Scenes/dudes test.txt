/////////////////////////////////////////
// Sixth version of the scene file format
// 
// - It allows you to add comments like this one
// - Syntax itself is hopefully self explanatory
// - Name of the objects and attributes are defined inside the executable

///////////////////////////////////////
//    Global scene and viewpoint     //
/////////////////////////////////////// 

Scene 
{
	// make sure the version and the executable match !
	Version.Major = 1;
	Version.Minor = 5;

	Camera.Position = -3.0, 15.0, -100.0;
	Camera.Rotation = 30.0;
	Camera.FieldOfView = 90.0;

	// Image Exposure
	Exposure = -2.5;
	
	Skybox.Material.Id = 0;

	// Count the objects in the scene
	NumberOfMaterials = 19;
	NumberOfSpheres = 0;
	NumberOfBoxes = 3;
	NumberOfLights = 2; 
	NumberOfModels = 0;
}




///////////////////////////////////////
//         List of lights            //
/////////////////////////////////////// 

Light0
{
  Position = 1000.0, 1000.0, -1000.0;
  Intensity = 0.5, 0.5, 0.5;
}
Light1
{
  Position = 0.0, 5.0, -1000.0;
  Intensity = 0.5, 0.5, 0.5;
}


///////////////////////////////////////
//         List of materials         //
/////////////////////////////////////// 
Material0
{
   Type = gouraud;
   //Reflection = 0.05;
   Diffuse = 0.6, 0.6, 1;
}
Material1
{
   Type = gouraud;
   Diffuse = 1, 0.8, 0.6;
}
Material2
{
   Type = gouraud;
   Diffuse = 0.3333333, 0, 0;
}
Material3
{
   Type = gouraud;
   Diffuse = 0.06666667, 0.06666667, 0.06666667;
}
Material4
{
   Type = gouraud;
   Diffuse = 0.8666667, 0.8666667, 0.8666667;
}
Material5
{
   Type = gouraud;
   Diffuse = 0.1333333, 0, 0;
}
Material6
{
   Type = gouraud;
   Diffuse = 0.4666667, 0.4666667, 0.4666667;
}
Material7
{
   Type = gouraud;
   Diffuse = 0.1333333, 0.1333333, 0.1333333;
}
Material8
{
   Type = gouraud;
   Diffuse = 0.3333333, 0.3333333, 0.3333333;
}
Material9
{
   Type = gouraud;
   Diffuse = 0.2666667, 0.2666667, 0.2666667;
}
Material10
{
   Type = gouraud;
   Diffuse = 0, 0.6, 0.2;
}
Material11
{
   Type = gouraud;
   Diffuse = 0, 0, 0.06666667;
}
Material12
{
   Type = gouraud;
   Diffuse = 0.7333333, 0, 0;
}
Material13
{
   Type = gouraud;
   Diffuse = 1, 0.4, 0;
}
Material14
{
   Type = gouraud;
   Diffuse = 0.6, 0.4, 0.2;
}
Material15
{
   Type = gouraud;
   Diffuse = 1, 0.6, 0.4;
}
Material16
{
   Type = gouraud;
   Diffuse = 0.6666667, 0.6666667, 0.6666667;
}
Material17
{
   Type = gouraud;
   Diffuse = 0.9333333, 0.9333333, 0.9333333;
}

Material18
{
   Type = checkerboard;

   Diffuse = 0.65, 0.65, 1;
   Diffuse2 = 0.55, 0.55, 1;
   Size = 5;
   Offset = 0, 3893343, 0;
   Reflection = 0.1;
}  

///////////////////////////////////////
//         List of models            //
/////////////////////////////////////// 

Model0
{
	Center = 0.0, 0.0, 0.0;
	Size = 1000;
	Normal = 0.0, 1.0, 0.0;
	Material.Id = 0;

	Triangles = 2;
	Triangle0 = 
		-1, 0, -1,
		-1, 0, 1,
		1, 0, -1;
	Triangle1 = 
		1, 0, -1,
		-1, 0, 1,
		1, 0, 1;
}
Model1
{
	Center = 0.0, 400.0, 400.0;
	Size = 400;
	Normal = 0.0, -1.0, 0.0;
	Material.Id = 0;

	Triangles = 2;
	Triangle0 = 
		-1, 0, -1,
		1, 0, -1,
		-1, 0, 1;
	Triangle1 = 
		-1, 0, 1,
		1, 0, -1,
		1, 0, 1;
}
Model2
{
	Center = -400.0, 0.0, 400.0;
	Size = 400;
	Normal = 1.0, 0.0, 0.0;
	Material.Id = 1;

	Triangles = 2;
	Triangle0 = 
		0, -1, -1,
		0, 1, -1,
		0, -1, 1;
	Triangle1 = 
		0, -1, 1,
		0, 1, -1,
		0, 1, 1;
}
Model3
{
	Center = 400.0, 0.0, 400.0;
	Size = 400;
	Normal = -1.0, 0.0, 0.0;
	Material.Id = 2;

	Triangles = 2;
	Triangle0 = 
		0, -1, -1,
		0, -1, 1,
		0, 1, -1;
	Triangle1 = 
		0, 1, -1,
		0, -1, 1,
		0, 1, 1;

}
Model4
{
	Center = 0.0, 0.0, 800.0;
	Size = 400;
	Normal = 0.0, 0.0, -1.0;
	Material.Id = 0;

	Triangles = 2;
	Triangle0 = 
		-1, -1, 0,
		-1, 1, 0,
		1, -1, 0;
	Triangle1 = 
		1, -1, 0,
		-1, 1, 0,
		1, 1, 0;
}


///////////////////////////////////////
//         List of spheres           //
/////////////////////////////////////// 
Sphere0
{
  Center = -200.0, -250.0, 450.0;
  Size = 150.0;
  Material.Id = 2;
}
Sphere1
{
  Center = 200.0, -250.0, 350.0;
  Size = 150.0;
  Material.Id = 3;
}

///////////////////////////////////////
//         List of boxes             //
/////////////////////////////////////// 
//Box0
//{
//  Point1 = -300.0, -350.0, 350.0;
//  Point2 = -100.0, -150.0, 550.0;
//  Material.Id = 3;
//}

//Box1
//{
//  Point1 = 50.0, -400.0, 200.0;
//  Point2 = 350.0, -100.0, 500.0;
//  Material.Id = 4;
//}
Box0
{
   Point1 = -10000, -1, -10000;
   Point2 = 10000, 0, 10000;
   Material.Id = 18;
}
Box1
{
   Point1 = 0, 0, 0;
   Point2 = 300, 300, 300;
   Material.Id = 2;
}
Box2
{
	Point1 = 0, 0, 0;
   	Point2 = 400, 400, 400;
   	Material.Id = 1;
}