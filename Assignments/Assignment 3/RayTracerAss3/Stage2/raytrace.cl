#include "sceneCL.h"
#include "ConstantsCL.h"
#include "IntersectionCL.h"
#include "Intersection.cl"


// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(__global const Scene* scene, Ray viewRay)
{
	Colour output = { 0.0f, 0.0f, 0.0f }; 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

	for (int level = 0; level < MAX_RAYS_CAST; ++level)				// loop until reached maximum ray cast limit (unless loop is broken out of)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (objectIntersection(scene, &viewRay, &intersect))
		{
			Colour temp = { 255.0f, 255.0f, 255.0f };
			output = temp;
			return output;
		}
	}

	return output;
}

void render(__global Scene* scene, const int width, const int height, const int aaLevel, int x, int y, __global unsigned int* out)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tan(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	Colour output = { 0.0f, 0.0f, 0.0f };

	// calculate multiple samples for each pixel
	const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

	// loop through all sub-locations within the pixel
	for (float fragmentx = (float)x - (width / 2); fragmentx < (x - (width / 2)) + 1.0f; fragmentx += sampleStep)
	{
		for (float fragmenty = (float)y - (height / 2); fragmenty < (y - (height / 2)) + 1.0f; fragmenty += sampleStep)
		{	
			// direction of default forward facing ray
			Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };

			// rotated direction of ray
			Vector rotatedDir = {
				dir.x * cos(scene->cameraRotation) - dir.z * sin(scene->cameraRotation),
				dir.y,
				dir.x * sin(scene->cameraRotation) + dir.z * cos(scene->cameraRotation) };

			// view ray starting from camera position and heading in rotated (normalised) direction
			Ray viewRay = { scene->cameraPosition, normalize(rotatedDir) };

			output += sampleRatio * traceRay(scene, viewRay);
		}
	}
	
	// store saturated final colour value in image buffer
	out[y * width + x] = convertToPixel(output);


	return;
}

__kernel void raytrace(__global Scene* scene, __global Material* materials, __global Light* lights, __global Sphere* spheres, __global Box* boxes, const int samples, __global unsigned int* out)
{
	unsigned int y = get_global_id(1);
	unsigned int x = get_global_id(0);

	unsigned int width = get_global_size(0);
	unsigned int height = get_global_size(1);

	scene->materialContainer = materials;
	scene->lightContainer = lights;
	scene->sphereContainer = spheres;
	scene->boxContainer = boxes;

	render(scene, width, height, samples, x, y, out);

	return;
}