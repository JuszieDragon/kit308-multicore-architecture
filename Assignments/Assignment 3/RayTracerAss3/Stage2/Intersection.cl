#include "IntersectionCL.h"

// test to see if collision between ray and a sphere happens before time t (equivalent to distance)
// updates closest collision time (/distance) if collision occurs
// see: http://en.wikipedia.org/wiki/Line-sphere_intersection
// see: http://www.codermind.com/articles/Raytracer-in-C++-Part-I-First-rays.html
// see: Step 8 of http://meatfighter.com/juggler/ 
// this code make heavy use of constant term removal due to ray always being a unit vector (i.e. normalised)
bool isSphereIntersected(__global const Sphere* s, const Ray* r, float* t)
{
	// Intersection of a ray and a sphere, check the articles for the rationale
	Vector dist = s->pos - r->start;
	float B = dot(r->dir, dist);
	float D = B * B - dot(dist, dist) + s->size * s->size;

	// if D < 0, no intersection, so don't try and calculate the point of intersection
	if (D < 0.0f) return false;

	// calculate both intersection times(/distances)
	float t0 = B - sqrt(D);
	float t1 = B + sqrt(D);

	// check to see if either of the two sphere collision points are closer than time parameter
	if ((t0 > EPSILON) && (t0 < *t))
	{
		*t = t0;
		return true;
	}
	else if ((t1 > EPSILON) && (t1 < *t))
	{
		*t = t1;
		return true;
	}

	return false;
}

// test to see if collision between ray and any object in the scene
// updates intersection structure if collision occurs
bool objectIntersection(__global const Scene* scene, const Ray* viewRay, Intersection* intersect)
{
	// set default distance to be a long long way away
	float t = MAX_RAY_DISTANCE;

	// no intersection found by default
	intersect->objectType = 0;

	// search for sphere collisions, storing closest one found
	for (unsigned int i = 0; i < scene->numSpheres; ++i)
	{
		if (isSphereIntersected(&scene->sphereContainer[i], viewRay, &t)) return true;
	}

	return false;

	//for (unsigned int i = 0; i < scene->numSpheres; ++i)
	//{
	//	if (isSphereIntersected(&scene->sphereContainer[i], viewRay, &t))
	//	{
	//		intersect->objectType = SPHERE;
	//		intersect->sphere = &scene->sphereContainer[i];
	//	}
	//}

	//// nothing detected, return false
	//if (intersect->objectType == NONE)
	//{
	//	return false;
	//}

	////return false;
	//// calculate the point of the intersection
	//intersect->pos = viewRay->start + viewRay->dir * t;

	return true;
}