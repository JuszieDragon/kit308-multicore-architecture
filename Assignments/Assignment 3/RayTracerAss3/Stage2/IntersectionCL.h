#ifndef __INTERSECTIONCL_H
#define __INTERSECTIONCL_H


#include "sceneCL.h"

// all pertinant information about an intersection of a ray with an object
typedef struct Intersection
{
	enum { NONE, SPHERE, BOX } objectType;				// type of object intersected with

	Point pos;											// point of intersection
	Vector normal;										// normal at point of intersection
	float viewProjection;								// view projection 
	bool insideObject;									// whether or not inside an object

	__global Material* material;									// material of object

	// object collided with
	union
	{
		__global struct Sphere* sphere;
		__global struct Box* box;
	};
} Intersection;

#endif //__INTERSECTIONCL_H