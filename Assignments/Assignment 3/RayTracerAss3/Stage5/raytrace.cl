#include "sceneCL.h"
#include "ConstantsCL.h"
#include "IntersectionCL.h"
#include "Intersection.cl"
#include "Lighting.cl"

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabs(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrt(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrt(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}

// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(__global const Scene* scene, Ray viewRay)
{
	Colour output = { 0.0f, 0.0f, 0.0f }; 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

	for (int level = 0; level < MAX_RAYS_CAST; ++level)				// loop until reached maximum ray cast limit (unless loop is broken out of)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (!objectIntersection(scene, &viewRay, &intersect)) break;

		// calculate response to collision: ie. get normal at point of collision and material of object
		calculateIntersectionResponse(scene, &viewRay, &intersect);

		// apply the diffuse and specular lighting 
		if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

		// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
		if (intersect.material->reflection)
		{
			viewRay = calculateReflection(&viewRay, &intersect);
			coef *= intersect.material->reflection;
		}
		else if (intersect.material->refraction)
		{
			viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
			coef *= intersect.material->refraction;
		}
		else
		{
			// if no reflection or refraction, then finish looping (cast no more rays)
			return output;
		}
	}

	// if the calculation coefficient is non-zero, read from the environment map
	if (coef > 0.0f)
	{
		Material currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

		output += coef * currentMaterial.diffuse;
	}

	return output;
}

void render(__global Scene* scene, const int width, const int height, const int aaLevel, const int x, const int y, __global unsigned int* out)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tan(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	Colour output = { 0.0f, 0.0f, 0.0f };

	// calculate multiple samples for each pixel
	const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

	// loop through all sub-locations within the pixel
	for (float fragmentx = (float)x; fragmentx < x + 1.0f; fragmentx += sampleStep)
	{
		for (float fragmenty = (float)y; fragmenty < y + 1.0f; fragmenty += sampleStep)
		{	
			// direction of default forward facing ray
			Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };

			// rotated direction of ray
			Vector rotatedDir = {
				dir.x * cos(scene->cameraRotation) - dir.z * sin(scene->cameraRotation),
				dir.y,
				dir.x * sin(scene->cameraRotation) + dir.z * cos(scene->cameraRotation) };

			// view ray starting from camera position and heading in rotated (normalised) direction
			Ray viewRay = { scene->cameraPosition, normalize(rotatedDir) };

			// follow ray and add proportional of the result to the final pixel colour
			output += sampleRatio * traceRay(scene, viewRay);
		}
	}

	// store saturated final colour value in image buffer
	*out = convertToPixel(output, scene->exposure);
	return;
}

__kernel void raytrace(__global Scene* scene, __global Material* materials, __global Light* lights, __global Sphere* spheres, __global Box* boxes, const int samples, const unsigned int width, const unsigned int height, __global unsigned int* out)
{
	// constants to account for the zero point being in the middle of the image
	const int y = get_global_id(1) - (height / 2);
	const int x = get_global_id(0) - (width / 2);

	const int trueY = get_global_id(1);
	const int trueX = get_global_id(0);

	scene->materialContainer = materials;
	scene->lightContainer = lights;
	scene->sphereContainer = spheres;
	scene->boxContainer = boxes;

	// calculate the array location of the start of the block
	__global unsigned int* output = out + (trueY * width) + trueX;

	render(scene, width, height, samples, x, y, output);

	return;
}