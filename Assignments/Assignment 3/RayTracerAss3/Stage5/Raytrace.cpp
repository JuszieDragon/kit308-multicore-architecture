﻿/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#define TARGET_WINDOWS

#pragma warning(disable: 4996)
#include <stdio.h>
#include "Timer.h"
#include "Primitives.h"
#include "Scene.h"
#include "Lighting.h"
#include "Intersection.h"
#include "ImageIO.h"
#include "LoadCL.h"

unsigned int buffer[MAX_WIDTH * MAX_HEIGHT];
// Buffer used to store all the kernals output in to construct the image
//unsigned int outputBuffer[MAX_WIDTH * MAX_HEIGHT];

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabsf(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrtf(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrtf(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}


// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(const Scene* scene, Ray viewRay)
{
	Colour output(0.0f, 0.0f, 0.0f); 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

																	// loop until reached maximum ray cast limit (unless loop is broken out of)
	for (int level = 0; level < MAX_RAYS_CAST; ++level)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (!objectIntersection(scene, &viewRay, &intersect)) break;

		// calculate response to collision: ie. get normal at point of collision and material of object
		calculateIntersectionResponse(scene, &viewRay, &intersect);

		// apply the diffuse and specular lighting 
		if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

		// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
		if (intersect.material->reflection)
		{
			viewRay = calculateReflection(&viewRay, &intersect);
			coef *= intersect.material->reflection;
		}
		else if (intersect.material->refraction)
		{
			viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
			coef *= intersect.material->refraction;
		}
		else
		{
			// if no reflection or refraction, then finish looping (cast no more rays)
			return output;
		}
	}

	// if the calculation coefficient is non-zero, read from the environment map
	if (coef > 0.0f)
	{
		Material& currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

		output += coef * currentMaterial.diffuse;
	}

	return output;
}

// render scene at given width and height and anti-aliasing level
int render(Scene* scene, const int width, const int height, const int aaLevel, bool testMode)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tanf(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	// pointer to output buffer
	unsigned int* out = buffer;

	// count of samples rendered
	unsigned int samplesRendered = 0;

	// loop through all the pixels
	for (int y = -height / 2; y < height / 2; ++y)
	{
		for (int x = -width / 2; x < width / 2; ++x)
		{
			Colour output(0.0f, 0.0f, 0.0f);

			// calculate multiple samples for each pixel
			const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

			// loop through all sub-locations within the pixel
			for (float fragmentx = float(x); fragmentx < x + 1.0f; fragmentx += sampleStep)
			{
				for (float fragmenty = float(y); fragmenty < y + 1.0f; fragmenty += sampleStep)
				{
					// direction of default forward facing ray
					Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };

					// rotated direction of ray
					Vector rotatedDir = {
						dir.x * cosf(scene->cameraRotation) - dir.z * sinf(scene->cameraRotation),
						dir.y,
						dir.x * sinf(scene->cameraRotation) + dir.z * cosf(scene->cameraRotation) };

					// view ray starting from camera position and heading in rotated (normalised) direction
					Ray viewRay = { scene->cameraPosition, normalise(rotatedDir) };

					// follow ray and add proportional of the result to the final pixel colour
					output += sampleRatio * traceRay(scene, viewRay);

					// count this sample
					samplesRendered++;
				}
			}

			if (!testMode)
			{
				// store saturated final colour value in image buffer
				*out++ = output.convertToPixel(scene->exposure);
			}
			else
			{
				// store colour (calculated from x,y coordinates) in image buffer 
				*out++ = Colour((x + width / 2) % 256 / 256.0f, 0, (y + height / 2) % 256 / 256.0f).convertToPixel();
			}
		}
	}

	return samplesRendered;
}

int gcd(int a, int b)
{
	if (a == b) return a / 4;
	
	int temp;

	while (b != 0)
	{
		temp = a % b;

		a = b;
		b = temp;
	}

	return a;
}

void timeFormat(int ms, int* hours, int* minutes, int* seconds, int* milliseconds)
{
	int temp = ms;

	*hours = temp / (1000 * 60 * 60);
	temp -= (*hours * (1000 * 60 * 60));

	*minutes = temp / (1000 * 60);
	temp -= (*minutes * (1000 * 60));

	*seconds = temp / 1000;
	temp -= (*seconds * 1000);

	*milliseconds = temp;

	return;
}

// read command line arguments, render, and write out BMP file
int main(int argc, char* argv[])
{
	//int width = 22340;
	int width = 22340;
	//int height = 22340;
	int height = 22340;
	int samples = 16;
	int blockSize = gcd(width, height);

	/*int test = 1354;
	int testHours, testMinutes, testSeconds, testMilliseconds;

	timeFormat(test, &testHours, &testMinutes, &testSeconds, &testMilliseconds);

	printf("%d hours, %d minutes, %d seconds, %d milliseconds.\nTime in milliseconds %d\n", testHours, testMinutes, testSeconds, testMilliseconds, test);

	return 0;*/

	// If the image has a low gcd/blocksize run a less efficent version of the raytracer that works with any blocksize
	bool lowGCD = false;
	if (blockSize < 500)
	{
		lowGCD = true;
		blockSize = fmax(width, height) / 4;
	}

	//blockSize = 512;

	// rendering options
	int runs = 1;
	bool testMode = false;

	// default input / output filenames
	const char* inputFilename = "Scenes/cornell-spiral-300lights.txt";

	char outputFilenameBuffer[1000];
	char* outputFilename = outputFilenameBuffer;

	// do stuff with command line args
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			width = atoi(argv[++i]);
			height = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-samples") == 0)
		{
			samples = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-input") == 0)
		{
			inputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-output") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-runs") == 0)
		{
			runs = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-testMode") == 0)
		{
			testMode = true;
		}
		else if (strcmp(argv[i], "-blockSize") == 0)
		{
			blockSize = atoi(argv[++i]);
		}
		else
		{
			fprintf(stderr, "unknown argument: %s\n", argv[i]);
		}
	}

	// nasty (and fragile) kludge to make an ok-ish default output filename (can be overriden with "-output" command line option)
	sprintf(outputFilenameBuffer, "Outputs/%s_%dx%dx%d_%s.bmp", (strrchr(inputFilename, '/') + 1), width, height, samples, (strrchr(argv[0], '\\') + 1));

	// read scene file
	Scene scene;
	if (!init(inputFilename, scene))
	{
		fprintf(stderr, "Failure when reading the Scene file.\n");
		return -1;
	}


	// stop caching issue, adds a fair chunk of time
	//_putenv_s("CUDA_CACHE_DISABLE", "1");

	Timer timer;	// create timer

	// declare openCL variables
	cl_int err;
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;
	cl_command_queue queue;
	cl_program program = NULL;
	cl_kernel kernel;
	cl_mem clBufferOut;
	cl_mem clBufferScene, clBufferMaterial, clBufferLight, clBufferSphere = NULL, clBufferBox = NULL;

	bool areSpheres = scene.numSpheres > 0 ? true : false;
	bool areBoxes = scene.numBoxes > 0 ? true : false;

	// calculate exactly how many blocks are needed (and deal with cases where the blockSize doesn't exactly divide)
	unsigned int blocksWide = (width - 1) / blockSize + 1;
	unsigned int blocksHigh = (height - 1) / blockSize + 1;
	unsigned int blocksTotal = blocksWide * blocksHigh;

	// store if the block size doesn't divide evenly into the width and/or height of the image 
	unsigned int xBlockOverhang = 0, yBlockOverhang = 0;

	// calculate how big the edge blocks need to be
	if (height % blockSize != 0)
	{
		yBlockOverhang = height - (blockSize * (blocksHigh - 1));
	}

	if (width % blockSize != 0)
	{
		xBlockOverhang = width - (blockSize * (blocksWide - 1));
	}

	//xBlockOverhang = 100;
	//yBlockOverhang = 250;

	int currentBlockY, currentBlockX = 0;

	printf("Blocksize: %d\nblocksWide: %d\nblocksHigh: %d\nxBlockOverhang: %d\nyBlockOverhang: %d\n\n", blockSize, blocksWide, blocksHigh, xBlockOverhang, yBlockOverhang);

	err = clGetPlatformIDs(1, &platform, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetPlatformIDs. Error code: %d\n", err);
		exit(1);
	}

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't find any devices\n");
		exit(1);
	}

	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create a context\n");
		exit(1);
	}

	queue = clCreateCommandQueue(context, device, 0, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create the command queue\n");
		exit(1);
	}

	/*if (lowGCD)
	{
		printf("using raytraceAnyBlockSize.cl\n");
		program = clLoadSource(context, "Stage5/raytraceAnyBlockSize.cl", &err);
		if (err != CL_SUCCESS)
		{
			printf("Couldn't load/create the program raytraceAnyBlockSize.cl\n");
			exit(1);
		}
	}
	else
	{*/
		//printf("using raytrace.cl\n");
		program = clLoadSource(context, "Stage5/raytrace.cl", &err);
		if (err != CL_SUCCESS)
		{
			printf("Couldn't load/create the program raytrace.cl\n");
			exit(1);
		}
	//}

	err = clBuildProgram(program, 0, NULL, "-I ./Stage5", NULL, NULL);
	if (err != CL_SUCCESS) 
	{
		char* program_log;
		size_t log_size;

		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		program_log = (char*)malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
		printf("%s\n", program_log);
		free(program_log);
		exit(1);
	}

	kernel = clCreateKernel(program, "raytrace", &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create the kernel\n");
		exit(1);
	}

	//create an appropriate buffer with clCreateBuffer
	//error check buffer creation
	clBufferScene = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Scene), &scene, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	clBufferOut = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(*buffer) * width * height, buffer, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create a bufferOut object\n");
		exit(1);
	}

	clBufferMaterial = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, scene.numMaterials * sizeof(Material), scene.materialContainer, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create a bufferMaterial object\n");
		exit(1);
	}

	clBufferLight = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, scene.numLights * sizeof(Light), scene.lightContainer, &err);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't create a bufferLight object\n");
		exit(1);
	}

	if (areSpheres)
	{
		clBufferSphere = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, scene.numSpheres * sizeof(Sphere), scene.sphereContainer, &err);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't create a bufferSphere object\n");
			exit(1);
		}
	}

	if (areBoxes)
	{
		clBufferBox = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, scene.numBoxes * sizeof(Box), scene.boxContainer, &err);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't create a bufferBox object\n");
			exit(1);
		}
	}

	//pass in all the kernel arguments (using clSetKernelArg)
	//(one argument per basic scalar -- alternatively these could be wrapped in a struct and passed in as one argument)
	//error check each kernel argument set
	err = clSetKernelArg(kernel, 0, sizeof(clBufferScene), &clBufferScene);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument Scene\n", 0);
		exit(1);
	}

	err = clSetKernelArg(kernel, 1, sizeof(clBufferMaterial), &clBufferMaterial);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument Material\n", 1);
		exit(1);
	}

	err = clSetKernelArg(kernel, 2, sizeof(clBufferLight), &clBufferLight);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument Light\n", 2);
		exit(1);
	}

	if (areSpheres)
	{
		err = clSetKernelArg(kernel, 3, sizeof(clBufferSphere), &clBufferSphere);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't set the kernel %d argument Sphere\n", 3);
			exit(1);
		}
	}
	else
	{
		err = clSetKernelArg(kernel, 3, 0, NULL);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't set the kernel %d argument Sphere\n", 3);
			exit(1);
		}
	}
	
	if (areBoxes)
	{
		err = clSetKernelArg(kernel, 4, sizeof(clBufferBox), &clBufferBox);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't set the kernel %d argument Box\n", 4);
			exit(1);
		}
	}
	else
	{
		err = clSetKernelArg(kernel, 4, 0, NULL);
		if (err != CL_SUCCESS) 
		{
			printf("Couldn't set the kernel %d argument Box\n", 4);
			exit(1);
		}
	}

	err = clSetKernelArg(kernel, 5, sizeof(samples), &samples);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument Samples\n", 5);
		exit(1);
	}
	
	err = clSetKernelArg(kernel, 6, sizeof(width), &width);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument blocksWide\n", 6);
		exit(1);
	}

	err = clSetKernelArg(kernel, 7, sizeof(height), &height);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument blocksHigh\n", 6);
		exit(1);
	}

	err = clSetKernelArg(kernel, 8, sizeof(clBufferOut), &clBufferOut);
	if (err != CL_SUCCESS) 
	{
		printf("Couldn't set the kernel %d argument Out\n", 7);
		exit(1);
	}

	// first time and total time taken to render all runs (used to calculate average)
	int firstTime = 0;
	int totalTime = 0;
	int samplesRendered = 0;

	Timer blockTimer;

	for (int i = 0; i < runs; i++)
	{
		if (i > 0) timer.start();

		for (int j = 0; j < blocksTotal; j++)
		{
			blockTimer.start();
			
			//printf("%d block\n", j);
			currentBlockY = j % blocksHigh;
			currentBlockX = j / blocksHigh;

			printf("currentBlock X and Y: %d %d\n", currentBlockX, currentBlockY);

			//size_t workOffset[] = { currentBlockY * blockSize, currentBlockX * blockSize };
			size_t workOffset[] = { currentBlockX * blockSize, currentBlockY * blockSize };
			//size_t workSize[];
			if (yBlockOverhang != 0 && currentBlockY == blocksHigh - 1)
			{
				printf("y check\n\n");
				
				size_t workSize[] = { blockSize, blockSize - yBlockOverhang };

				err = clEnqueueNDRangeKernel(queue, kernel, 2, workOffset, workSize, NULL, 0, NULL, NULL);
				if (err != CL_SUCCESS) {
					printf("Couldn't enqueue the kernel execution command\n");
					exit(1);
				}
			}
			else if (xBlockOverhang != 0 && currentBlockX == blocksWide - 1)
			{
				printf("x check\n\n");

				size_t workSize[] = { blockSize - xBlockOverhang, blockSize };

				err = clEnqueueNDRangeKernel(queue, kernel, 2, workOffset, workSize, NULL, 0, NULL, NULL);
				if (err != CL_SUCCESS) {
					printf("Couldn't enqueue the kernel execution command\n");
					exit(1);
				}
			}
			else
			{
				size_t workSize[] = { blockSize, blockSize };

				err = clEnqueueNDRangeKernel(queue, kernel, 2, workOffset, workSize, NULL, 0, NULL, NULL);
				if (err != CL_SUCCESS) {
					printf("Couldn't enqueue the kernel execution command\n");
					exit(1);
				}
			}
			
			/*err = clEnqueueNDRangeKernel(queue, kernel, 2, workOffset, workSize, NULL, 0, NULL, NULL);
			if (err != CL_SUCCESS) {
				printf("Couldn't enqueue the kernel execution command\n");
				exit(1);
			}*/

			//read the results with clEnqueueReadBuffer
			//error check the read
			err = clEnqueueReadBuffer(queue, clBufferOut, CL_TRUE, 0, sizeof(*buffer) * width * height, buffer, 0, NULL, NULL);
			if (err != CL_SUCCESS)
			{
				printf("\nError calling clEnqueueReadBuffer after time %d. Error code: %d\n", j, err);
				exit(1);
			}

			blockTimer.end();
			int blockHours, blockMinutes, blockSeconds, blockMilliseconds;
			timeFormat(blockTimer.getMilliseconds(), &blockHours, &blockMinutes, &blockSeconds, &blockMilliseconds);

			printf("block %d %d run time: %d hours, %d minutes, %d seconds, %d milliseconds.\n\n", currentBlockX, currentBlockY, blockHours, blockMinutes, blockSeconds, blockMilliseconds);
		}

		timer.end();	// record end time
		if (i > 0)
		{
			totalTime += timer.getMilliseconds();	// record total time taken
		}
		else
		{
			firstTime = timer.getMilliseconds();	// record first time taken
		}
	}

	int hours, minutes, seconds, milliseconds;

	// output timing information (first run, runs run and average)
	if (runs > 1)
	{
		timeFormat(firstTime, &hours, &minutes, &seconds, &milliseconds);
		//printf("first run time: %dms, subsequent average time taken (%d run(s)): %.1fms\n", firstTime, runs - 1, (totalTime - firstTime) / (float)(runs - 1));
		printf("first run time: %d hours, %d minutes, %d seconds, %d milliseconds. Subsequent average time taken (%d run(s)): %.1fms\n", hours, minutes, seconds, milliseconds,  runs - 1, (totalTime - firstTime) / (float)(runs - 1));
	}
	else
	{
		timeFormat(firstTime, &hours, &minutes, &seconds, &milliseconds);
		//printf("first run time: %dms, subsequent average time taken (%d run(s)): N/A\n", firstTime, runs - 1);
		printf("first run time: %d hours, %d minutes, %d seconds, %d milliseconds. Subsequent average time taken (%d run(s)): N/A\n", hours, minutes, seconds, milliseconds, runs - 1);
	}


	//OpenCL cleanup time
	clReleaseMemObject(clBufferOut);
	if (areSpheres) clReleaseMemObject(clBufferSphere);
	if (areBoxes) clReleaseMemObject(clBufferBox);
	clReleaseMemObject(clBufferLight);
	clReleaseMemObject(clBufferMaterial);

	clReleaseCommandQueue(queue);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseContext(context);

	// output BMP file
	write_bmp(outputFilename, buffer, width, height, width);
	printf("Finished outputting image\n");
}