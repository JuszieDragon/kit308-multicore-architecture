typedef float3 Point;
typedef float3 Vector;
typedef float3 Colour;

typedef struct Ray
{
	Point start;
	Vector dir;
} Ray;