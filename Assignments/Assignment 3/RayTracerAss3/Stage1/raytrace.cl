#include "sceneCL.h"

void outputInfo(const Scene* scene, __global Material* materials, __global Light* lights, __global Sphere* spheres, __global Box* boxes)
{
	printf("\n---- GPU --------\n");
	printf("sizeof(Point):    %d\n", sizeof(Point));
	printf("sizeof(Vector):   %d\n", sizeof(Vector));
	printf("sizeof(Colour):   %d\n", sizeof(Colour));
	printf("sizeof(Ray):      %d\n", sizeof(Ray));
	printf("sizeof(Light):    %d\n", sizeof(Light));
	printf("sizeof(Sphere):   %d\n", sizeof(Sphere));
	printf("sizeof(Box):      %d\n", sizeof(Box));
	printf("sizeof(Material): %d\n", sizeof(Material));
	printf("sizeof(Scene):    %d\n", sizeof(Scene));

	printf("\n--- Scene:\n");;
	printf("pos: %.1f %.1f %.1f\n", scene->cameraPosition.s0, scene->cameraPosition.s1, scene->cameraPosition.s2);
	printf("rot: %.1f\n", scene->cameraRotation);
	printf("fov: %.1f\n", scene->cameraFieldOfView);
	printf("exp: %.1f\n", scene->exposure);
	printf("sky: %d\n", scene->skyboxMaterialId);

	printf("\n--- Spheres (%d):\n", scene->numSpheres);;
	for (unsigned int i = 0; i < scene->numSpheres; ++i)
	{
		if (scene->numSpheres > 10 && i >= 3 && i < scene->numSpheres - 3)
		{
			printf(" ... \n");
			i = scene->numSpheres - 3;
			continue;
		}

		printf("Sphere %d: %.1f %.1f %.1f, %.1f -- %d\n", i, spheres[i].pos.s0, spheres[i].pos.s1, spheres[i].pos.s2, spheres[i].size, spheres[i].materialId);
	}

	printf("\n--- Boxes (%d):\n", scene->numBoxes);
	for (unsigned int i = 0; i < scene->numBoxes; ++i)
	{
		if (scene->numBoxes > 10 && i >= 3 && i < scene->numBoxes - 3)
		{
			printf(" ... \n");
			i = scene->numBoxes - 3;
			continue;
		}

		printf("Box %d: %.1f %.1f %.1f, %.1f %.1f %.1f -- %d\n", i,
			boxes[i].p1.x, boxes[i].p1.y, boxes[i].p1.z,
			boxes[i].p2.x, boxes[i].p2.y, boxes[i].p2.z,
			boxes[i].materialId
		);
	}

	printf("\n--- Lights (%d):\n", scene->numLights);
	for (unsigned int i = 0; i < scene->numLights; ++i)
	{
		if (scene->numLights > 10 && i >= 3 && i < scene->numLights - 3)
		{
			printf(" ... \n");
			i = scene->numLights - 3;
			continue;
		}

		printf("Light %d: %.1f %.1f %.1f -- %.1f %.1f %.1f\n", i,
			lights[i].pos.x, lights[i].pos.y, lights[i].pos.z,
			lights[i].intensity.s0, lights[i].intensity.s1, lights[i].intensity.s2);
	}

	printf("\n--- Materials (%d):\n", scene->numMaterials);
	for (unsigned int i = 0; i < scene->numMaterials; ++i)
	{
		if (scene->numMaterials > 10 && i >= 3 && i < scene->numMaterials - 3)
		{
			printf(" ... \n");
			i = scene->numMaterials - 3;
			continue;
		}

		printf("Material %d: %d %.1f %.1f %.1f ... %.1f %.1f %.1f\n", i,
			materials[i].type,
			materials[i].diffuse.s0, materials[i].diffuse.s1, materials[i].diffuse.s2,
			materials[i].reflection,
			materials[i].refraction,
			materials[i].density);
	}
}

__kernel void raytrace(Scene scene, __global Material* materials, __global Light* lights, __global Sphere* spheres, __global Box* boxes, __global unsigned int* out)
{
	unsigned int y = get_global_id(1);
	unsigned int x = get_global_id(0);

	unsigned int width = get_global_size(0);
	unsigned int height = get_global_size(1);

	unsigned int outputColour = x % 256;			// red channel
	//outputColour = (outputColour << 8) | 0;		// green channel
	outputColour = (outputColour << 16) | y % 256;	// blue channel (increased to 16 cause we don't care about green or alpha channel)
	//outputColour = (outputColour << 8) | 0;		// alpha channel

	//out[y * width + x] = outputColour;
	out[y * width + x] = (x % 256) & (y % 256);

	return;
}