/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#define TARGET_WINDOWS

#pragma warning(disable: 4996)
#include <stdio.h>
#include "Timer.h"
#include "Primitives.h"
#include "Scene.h"
#include "Lighting.h"
#include "Intersection.h"
#include "ImageIO.h"
#include "LoadCL.h"

unsigned int buffer[MAX_WIDTH * MAX_HEIGHT];

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabsf(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrtf(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrtf(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}


// follow a single ray until it's final destination (or maximum number of steps reached)
Colour traceRay(const Scene* scene, Ray viewRay)
{
	Colour output(0.0f, 0.0f, 0.0f); 								// colour value to be output
	float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
	float coef = 1.0f;												// amount of ray left to transmit
	Intersection intersect;											// properties of current intersection

																	// loop until reached maximum ray cast limit (unless loop is broken out of)
	for (int level = 0; level < MAX_RAYS_CAST; ++level)
	{
		// check for intersections between the view ray and any of the objects in the scene
		// exit the loop if no intersection found
		if (!objectIntersection(scene, &viewRay, &intersect)) break;

		// calculate response to collision: ie. get normal at point of collision and material of object
		calculateIntersectionResponse(scene, &viewRay, &intersect);

		// apply the diffuse and specular lighting 
		if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

		// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
		if (intersect.material->reflection)
		{
			viewRay = calculateReflection(&viewRay, &intersect);
			coef *= intersect.material->reflection;
		}
		else if (intersect.material->refraction)
		{
			viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
			coef *= intersect.material->refraction;
		}
		else
		{
			// if no reflection or refraction, then finish looping (cast no more rays)
			return output;
		}
	}

	// if the calculation coefficient is non-zero, read from the environment map
	if (coef > 0.0f)
	{
		Material& currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

		output += coef * currentMaterial.diffuse;
	}

	return output;
}

// render scene at given width and height and anti-aliasing level
int render(Scene* scene, const int width, const int height, const int aaLevel, bool testMode)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tanf(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	// pointer to output buffer
	unsigned int* out = buffer;

	// count of samples rendered
	unsigned int samplesRendered = 0;

	// loop through all the pixels
	for (int y = -height / 2; y < height / 2; ++y)
	{
		for (int x = -width / 2; x < width / 2; ++x)
		{
			Colour output(0.0f, 0.0f, 0.0f);

			// calculate multiple samples for each pixel
			const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

			// loop through all sub-locations within the pixel
			for (float fragmentx = float(x); fragmentx < x + 1.0f; fragmentx += sampleStep)
			{
				for (float fragmenty = float(y); fragmenty < y + 1.0f; fragmenty += sampleStep)
				{
					// direction of default forward facing ray
					Vector dir = { fragmentx * dirStepSize, fragmenty * dirStepSize, 1.0f };

					// rotated direction of ray
					Vector rotatedDir = {
						dir.x * cosf(scene->cameraRotation) - dir.z * sinf(scene->cameraRotation),
						dir.y,
						dir.x * sinf(scene->cameraRotation) + dir.z * cosf(scene->cameraRotation) };

					// view ray starting from camera position and heading in rotated (normalised) direction
					Ray viewRay = { scene->cameraPosition, normalise(rotatedDir) };

					// follow ray and add proportional of the result to the final pixel colour
					output += sampleRatio * traceRay(scene, viewRay);

					// count this sample
					samplesRendered++;
				}
			}

			if (!testMode)
			{
				// store saturated final colour value in image buffer
				*out++ = output.convertToPixel(scene->exposure);
			}
			else
			{
				// store colour (calculated from x,y coordinates) in image buffer 
				*out++ = Colour((x + width / 2) % 256 / 256.0f, 0, (y + height / 2) % 256 / 256.0f).convertToPixel();
			}
		}
	}

	return samplesRendered;
}

// read command line arguments, render, and write out BMP file
int main(int argc, char* argv[])
{
	int width = 1024;
	int height = 1024;
	int samples = 1;

	// rendering options
	int times = 1;
	bool testMode = false;

	// default input / output filenames
	const char* inputFilename = "Scenes/cornell.txt";

	char outputFilenameBuffer[1000];
	char* outputFilename = outputFilenameBuffer;

	// do stuff with command line args
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			width = atoi(argv[++i]);
			height = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-samples") == 0)
		{
			samples = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-input") == 0)
		{
			inputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-output") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-runs") == 0)
		{
			times = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-testMode") == 0)
		{
			testMode = true;
		}
		else
		{
			fprintf(stderr, "unknown argument: %s\n", argv[i]);
		}
	}

	// nasty (and fragile) kludge to make an ok-ish default output filename (can be overriden with "-output" command line option)
	sprintf(outputFilenameBuffer, "Outputs/%s_%dx%dx%d_%s.bmp", (strrchr(inputFilename, '/') + 1), width, height, samples, (strrchr(argv[0], '\\') + 1));

	// read scene file
	Scene scene;
	if (!init(inputFilename, scene))
	{
		fprintf(stderr, "Failure when reading the Scene file.\n");
		return -1;
	}

	//printf("starting opencl setup\n");

	// stop caching issue, adds a fair chunk of time
	_putenv_s("CUDA_CACHE_DISABLE", "1");

	Timer timer;	// create timer

	// declare openCL variables
	cl_int err;
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;
	cl_command_queue queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem clBufferOut;
	cl_mem clBufferScene, clBufferMaterial, clBufferLight, clBufferSphere, clBufferBox;

	// start at 0,0
	size_t workOffset[] = { 0, 0 };

	// rendering a width x height image
	size_t workSize[] = { width, height };


	err = clGetPlatformIDs(1, &platform, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetPlatformIDs. Error code: %d\n", err);
		exit(1);
	}

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	if (err != CL_SUCCESS) {
		printf("Couldn't find any devices\n");
		exit(1);
	}

	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a context\n");
		exit(1);
	}

	queue = clCreateCommandQueue(context, device, 0, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create the command queue\n");
		exit(1);
	}

	program = clLoadSource(context, "Stage3/raytrace.cl", &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't load/create the program\n");
		exit(1);
	}

	err = clBuildProgram(program, 0, NULL, "-I ./Stage3", NULL, NULL);
	if (err != CL_SUCCESS) {
		char* program_log;
		size_t log_size;

		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		program_log = (char*)malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
		printf("%s\n", program_log);
		free(program_log);
		exit(1);
	}

	kernel = clCreateKernel(program, "raytrace", &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create the kernel\n");
		exit(1);
	}

	//create an appropriate buffer with clCreateBuffer
	//error check buffer creation
	clBufferScene = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Scene), &scene, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	clBufferOut = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(*buffer) * width * height, buffer, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferOut object\n");
		exit(1);
	}

	clBufferMaterial = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, sizeof(scene.materialContainer) * scene.numMaterials + scene.numMaterials * sizeof(Material), scene.materialContainer, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferMaterial object\n");
		exit(1);
	}

	clBufferLight = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, sizeof(scene.lightContainer) * scene.numLights + scene.numLights * sizeof(Light), scene.lightContainer, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferLight object\n");
		exit(1);
	}

	// these buffers are increased by one to deal with any scene that has no boxes and/or spheres as the buffer doesn't like that
	clBufferSphere = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, sizeof(scene.sphereContainer) * scene.numSpheres + (scene.numSpheres + (unsigned long long)1) * sizeof(Sphere), scene.sphereContainer, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferSphere object\n");
		exit(1);
	}

	clBufferBox = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, sizeof(scene.boxContainer) * scene.numBoxes + (scene.numBoxes + (unsigned long long)1) * sizeof(Box), scene.boxContainer, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferBox object\n");
		exit(1);
	}

	int argNum = 0;

	//pass in all the kernel arguments (using clSetKernelArg)
	//(one argument per basic scalar -- alternatively these could be wrapped in a struct and passed in as one argument)
	//error check each kernel argument set
	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferScene), &clBufferScene);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Scene\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferMaterial), &clBufferMaterial);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Material\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferLight), &clBufferLight);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Light\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferSphere), &clBufferSphere);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Sphere\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferBox), &clBufferBox);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Box\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(samples), &samples);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Samples\n", argNum);
		exit(1);
	}

	err = clSetKernelArg(kernel, argNum++, sizeof(clBufferOut), &clBufferOut);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel %d argument Out\n", argNum);
		exit(1);
	}

	// first time and total time taken to render all runs (used to calculate average)
	int firstTime = 0;
	int totalTime = 0;
	int samplesRendered = 0;
	for (int i = 0; i < times; i++)
	{
		if (i > 0) timer.start();

		err = clEnqueueNDRangeKernel(queue, kernel, 2, workOffset, workSize, NULL, 0, NULL, NULL);
		if (err != CL_SUCCESS) {
			printf("Couldn't enqueue the kernel execution command\n");
			exit(1);
		}

		//read the results with clEnqueueReadBuffer
		//error check the read
		err = clEnqueueReadBuffer(queue, clBufferOut, CL_TRUE, 0, sizeof(*buffer) * width * height, buffer, 0, NULL, NULL);
		if (err != CL_SUCCESS)
		{
			printf("\nError calling clEnqueueReadBuffer. Error code: %d\n", err);
			exit(1);
		}

		timer.end();																					// record end time
		if (i > 0)
		{
			totalTime += timer.getMilliseconds();														// record total time taken
		}
		else
		{
			firstTime = timer.getMilliseconds();														// record first time taken
		}
	}

	// output timing information (first run, times run and average)
	if (times > 1)
	{
		printf("first run time: %dms, subsequent average time taken (%d run(s)): %.1fms\n", firstTime, times - 1, totalTime / (float)(times - 1));
	}
	else
	{
		printf("first run time: %dms, subsequent average time taken (%d run(s)): N/A\n", firstTime, times - 1);
	}


	//OpenCL cleanup time
	clReleaseMemObject(clBufferOut);
	clReleaseMemObject(clBufferSphere);
	clReleaseMemObject(clBufferBox);
	clReleaseMemObject(clBufferLight);
	clReleaseMemObject(clBufferMaterial);

	clReleaseCommandQueue(queue);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseContext(context);

	// output BMP file
	write_bmp(outputFilename, buffer, width, height, width);
}