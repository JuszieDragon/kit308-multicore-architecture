#include "PrimativesCL.h"
#include "IntersectionCL.h"

// apply computed checkerboard texture
Colour applyCheckerboard(const Intersection* intersect)
{
	Point p = (intersect->pos - intersect->material->offset) / intersect->material->size;

	int which = ((int)floor(p.x) + (int)floor(p.y) + (int)floor(p.z)) & 1;

	return (which ? intersect->material->diffuse : intersect->material->diffuse2);
}


// apply computed circular texture
Colour applyCircles(const Intersection* intersect)
{
	Point p = (intersect->pos - intersect->material->offset) / intersect->material->size;

	int which = (int)floor(sqrt(p.x * p.x + p.y * p.y + p.z * p.z)) & 1;

	return (which ? intersect->material->diffuse : intersect->material->diffuse2);
}


// apply computed wood grain texture
Colour applyWood(const Intersection* intersect)
{
	//Point p = (intersect->pos - intersect->material->offset) / intersect->material->size;
	Point temp = (intersect->pos - intersect->material->offset) / intersect->material->size;

	// squiggle up where the point is
	//p = { p.x * cos(p.y * 0.996f) * sin(p.z * 1.023f), cos(p.x) * p.y * sin(p.z * 1.211f), cos(p.x * 1.473f) * cos(p.y * 0.795f) * p.z };
	Point p = { temp.x * cos(temp.y * 0.996f) * sin(temp.z * 1.023f), cos(temp.x) * temp.y * sin(temp.z * 1.211f), cos(temp.x * 1.473f) * cos(temp.y * 0.795f) * temp.z };

	int which = (int)floor(sqrt(p.x * p.x + p.y * p.y + p.z * p.z)) & 1;

	return (which ? intersect->material->diffuse : intersect->material->diffuse2);
}