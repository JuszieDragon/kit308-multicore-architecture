#ifndef __PRIMATIVESCL_H
#define __PRIMATIVESCL_H

typedef float3 Point;
typedef float3 Vector;
typedef float3 Colour;

inline Vector minus (const Point p1, const Point p2)
{
	Vector v = { p1.x - p2.x, p1.y - p2.y, p1.z - p2.z };
	return v;
}

// convert colour to pixel (in 0x00BBGGRR format) with respect to an exposure level 
inline unsigned int convertToPixel(Colour c, float exposure)
{
	return ((unsigned char)(255 * (1.0f - exp(c.s2 * exposure))) << 16) +
		((unsigned char)(255 * (1.0f - exp(c.s1 * exposure))) << 8) +
		((unsigned char)(255 * (1.0f - exp(c.s0 * exposure))) << 0);
}

typedef struct Ray
{
	Point start;
	Vector dir;
} Ray;

#endif // __PRIMATIVESCL_H