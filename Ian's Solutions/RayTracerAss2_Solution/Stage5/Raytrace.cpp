/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#define TARGET_WINDOWS

#pragma warning(disable: 4996)
#include "Timer.h"
#include "Primitives.h"
#include "Scene.h"
#include "Lighting.h"
#include "Intersection.h"
#include "ImageIO.h"
#include "Raytrace.h"
#include "PrimitivesSIMD.h"

unsigned int buffer[MAX_WIDTH * MAX_HEIGHT];

// reflect the ray from an object
Ray calculateReflection(const Ray* viewRay, const Intersection* intersect)
{
	// reflect the viewRay around the object's normal
	Ray newRay = { intersect->pos, viewRay->dir - (intersect->normal * intersect->viewProjection * 2.0f) };

	return newRay;
}


// refract the ray through an object
Ray calculateRefraction(const Ray* viewRay, const Intersection* intersect, float* currentRefractiveIndex)
{
	// change refractive index depending on whether we are in an object or not
	float oldRefractiveIndex = *currentRefractiveIndex;
	*currentRefractiveIndex = intersect->insideObject ? DEFAULT_REFRACTIVE_INDEX : intersect->material->density;

	// calculate refractive ratio from old index and current index
	float refractiveRatio = oldRefractiveIndex / *currentRefractiveIndex;

	// Here we take into account that the light movement is symmetrical from the observer to the source or from the source to the oberver.
	// We then do the computation of the coefficient by taking into account the ray coming from the viewing point.
	float fCosThetaT;
	float fCosThetaI = fabsf(intersect->viewProjection);

	// glass-like material, we're computing the fresnel coefficient.
	if (fCosThetaI >= 1.0f)
	{
		// In this case the ray is coming parallel to the normal to the surface
		fCosThetaT = 1.0f;
	}
	else
	{
		float fSinThetaT = refractiveRatio * sqrtf(1 - fCosThetaI * fCosThetaI);

		// Beyond the angle (1.0f) all surfaces are purely reflective
		fCosThetaT = (fSinThetaT * fSinThetaT >= 1.0f) ? 0.0f : sqrtf(1 - fSinThetaT * fSinThetaT);
	}

	// Here we compute the transmitted ray with the formula of Snell-Descartes
	Ray newRay = { intersect->pos, (viewRay->dir + intersect->normal * fCosThetaI) * refractiveRatio - (intersect->normal * fCosThetaT) };

	return newRay;
}


// SIMD stub version of this function (returns SIMD values, but uses fully scalar calculations internally)
// follow a single ray until it's final destination (or maximum number of steps reached)
Colour8 traceRay(const Scene* scene, Vector8 viewRayStart, Vector8 viewRayDir)
{
	Colour8 outputs(0.0f, 0.0f, 0.0f); 									// colour value to be output

	for (int i = 0; i < 8; i++)
	{
		Colour output(0.0f, 0.0f, 0.0f);
		float currentRefractiveIndex = DEFAULT_REFRACTIVE_INDEX;		// current refractive index
		float coef = 1.0f;												// amount of ray left to transmit
		Intersection intersect;											// properties of current intersection

		Ray viewRay;
		viewRay.start = { viewRayStart.xs.m256_f32[i], viewRayStart.ys.m256_f32[i], viewRayStart.zs.m256_f32[i] };
		viewRay.dir = { viewRayDir.xs.m256_f32[i], viewRayDir.ys.m256_f32[i], viewRayDir.zs.m256_f32[i] };

		// loop until reached maximum ray cast limit (unless loop is broken out of)
		for (int level = 0; level < MAX_RAYS_CAST; ++level)
		{
			// check for intersections between the view ray and any of the objects in the scene
			// exit the loop if no intersection found
			if (!objectIntersection(scene, &viewRay, &intersect)) break;

			// calculate response to collision: ie. get normal at point of collision and material of object
			calculateIntersectionResponse(scene, &viewRay, &intersect);

			// apply the diffuse and specular lighting 
			if (!intersect.insideObject) output += coef * applyLighting(scene, &viewRay, &intersect);

			// if object has reflection or refraction component, adjust the view ray and coefficent of calculation and continue looping
			if (intersect.material->reflection)
			{
				viewRay = calculateReflection(&viewRay, &intersect);
				coef *= intersect.material->reflection;
			}
			else if (intersect.material->refraction)
			{
				viewRay = calculateRefraction(&viewRay, &intersect, &currentRefractiveIndex);
				coef *= intersect.material->refraction;
			}
			else
			{
				// if no reflection or refraction, then finish looping (cast no more rays)
				coef = 0.0f;
				break;
				//return output;
			}
		}

		// if the calculation coefficient is non-zero, read from the environment map
		if (coef > 0.0f)
		{
			Material& currentMaterial = scene->materialContainer[scene->skyboxMaterialId];

			output += coef * currentMaterial.diffuse;
		}

		outputs.reds.m256_f32[i] = output.red;
		outputs.greens.m256_f32[i] = output.green;
		outputs.blues.m256_f32[i] = output.blue;
	}


	return outputs;
}

// render scene at given width and height and anti-aliasing level
unsigned int renderSection(Scene* scene, const int width, const int height, const int aaLevel, const int blockSize,
	const bool testMode, const float testColour, const unsigned int colourMask,
	unsigned int* out, unsigned int* currentBlockShared)
{
	// angle between each successive ray cast (per pixel, anti-aliasing uses a fraction of this)
	const float dirStepSize = 1.0f / (0.5f * width / tanf(PIOVER180 * 0.5f * scene->cameraFieldOfView));

	// calculate exactly how many blocks are needed (and deal with cases where the blockSize doesn't exactly divide)
	unsigned int blocksWide = (width - 1) / blockSize + 1;
	unsigned int blocksHigh = (height - 1) / blockSize + 1;
	unsigned int blocksTotal = blocksWide * blocksHigh;

	// count of samples rendered
	unsigned int samplesRendered = 0;

	// current block index
	unsigned int currentBlock;

	while ((currentBlock = InterlockedIncrement(currentBlockShared)) < blocksTotal)
	{
		// block x,y position
		const int bx = currentBlock % blocksWide;
		const int by = currentBlock / blocksWide;

		// block coordinates (making sure not to exceed image bounds with non-divisible block sizes)
		const int xMin = bx * blockSize - width / 2;
		const int xMax = (std::min)(xMin + blockSize, width / 2);
		const int yMin = by * blockSize - height / 2;
		const int yMax = (std::min)(yMin + blockSize, height / 2);

		// debug
		//printf("thread rendering: block %d [%d,%d] (%d,%d)->(%d,%d) => %p\n", currentBlock, bx, by, xMin, yMin, xMax, yMax, out);

		// calculate the array location of the start of the block
		unsigned int* outBlock = buffer + width * by * blockSize + bx * blockSize;

		// jump required to get to the start of the next line of the block
		unsigned int outJump = width - (xMax - xMin);

		// loop through all the pixels
		for (int y = yMin; y < yMax; ++y)
		{
			for (int x = xMin; x < xMax; x += 8)
			{
				Colour8 outputs(0.0f, 0.0f, 0.0f);

				// calculate multiple samples for each pixel
				const float sampleStep = 1.0f / aaLevel, sampleRatio = 1.0f / (aaLevel * aaLevel);

				__m256 xOffsets = _mm256_setr_ps(0, 1, 2, 3, 4, 5, 6, 7);

				// loop through all sub-locations within the pixel
				for (float fragmentx = float(x); fragmentx < x + 1.0f; fragmentx += sampleStep)
				{
					for (float fragmenty = float(y); fragmenty < y + 1.0f; fragmenty += sampleStep)
					{
						__m256 fragmentxs = _mm256_set1_ps(fragmentx) + xOffsets;

						// direction of default forward facing ray
						Vector8 dir(fragmentxs * _mm256_set1_ps(dirStepSize), _mm256_set1_ps(fragmenty * dirStepSize), _mm256_set1_ps(1.0f));

						Vector8 rotatedDirs(
							dir.xs * _mm256_set1_ps(cos(scene->cameraRotation)) - dir.zs * _mm256_set1_ps(sinf(scene->cameraRotation)),
							dir.ys,
							dir.xs * _mm256_set1_ps(sinf(scene->cameraRotation)) + dir.zs * _mm256_set1_ps(cosf(scene->cameraRotation)));

						Vector8 viewRayStart(scene->cameraPosition.x, scene->cameraPosition.y, scene->cameraPosition.z);
						Vector8 viewRayDir(normalise(rotatedDirs));

						outputs = outputs + _mm256_set1_ps(sampleRatio) * traceRay(scene, viewRayStart, viewRayDir);

						// count this sample
						samplesRendered += 8;
					}
				}

				if (!testMode)
				{
					if (colourMask != 0) outputs.colourise(colourMask);

					// store saturated final colour value in image buffer
					_mm256_store_si256((__m256i*) outBlock, outputs.convertToPixel(scene->exposure));
				}
				else
				{
					// store white in image buffer (with multiple threads this should store a grey based on the thread number)
					_mm256_store_si256((__m256i*) outBlock, Colour8(testColour, testColour, testColour).convertToPixel());
				}
				outBlock += 8;
			}

			outBlock += outJump;
		}
	}

	return samplesRendered;
}


struct ThreadParams
{
	Scene* scene;
	int width;
	int height;
	int aaLevel;
	int blockSize;
	bool testMode;
	float testColour;
	unsigned int* out;
	unsigned int colourMask;
	unsigned int* currentBlocKShared;
};


// thread callback for rendering
DWORD __stdcall renderSectionThread(LPVOID inData)
{
	// cast the void* structure to something useful
	ThreadParams* params = (ThreadParams*)inData;

	// call the real render function
	int samplesRendered = renderSection(params->scene, params->width, params->height, params->aaLevel, params->blockSize,
		params->testMode, params->testColour, params->colourMask, params->out, params->currentBlocKShared);

	// exit with success
	ExitThread(samplesRendered);
}


// render scene at given width and height and anti-aliasing level using a specified number of threads
unsigned int render(const unsigned int threadCount, Scene* scene, const int width, const int height, const int aaLevel, const int blockSize, const bool testMode, const bool colourise)
{
	// count of samples rendered
	unsigned int samplesRendered = 0;

	// reserve space for threads and their parameters
	HANDLE* threads = new HANDLE[threadCount];
	ThreadParams* params = new ThreadParams[threadCount];

	// one less than the current block to render (shared between threads)
	unsigned int currentBlockShared = -1;

	// loop through all the squares
	for (unsigned int i = 0; i < threadCount; ++i)
	{
		// set up thread parameters
		params[i] = { scene, width, height, aaLevel, blockSize, testMode, i / (float)threadCount, buffer, colourise ? (i % 8) : 0, &currentBlockShared };

		// start thread
		threads[i] = CreateThread(NULL, 0, renderSectionThread, (LPVOID)&params[i], 0, NULL);
	}

	// wait until all the threads are done
	for (unsigned int i = 0; i < threadCount; i++) {
		WaitForSingleObject(threads[i], INFINITE);
		DWORD retValue;
		if (GetExitCodeThread(threads[i], &retValue))
		{
			samplesRendered += retValue;
		}
	}

	// clean up thread and param storage
	delete[] params;
	delete[] threads;

	// return cumulative samples rendered value
	return samplesRendered;
}



// allocate space fro SoA, and copy values from AoS to SoA 
void simdifySceneContainers(Scene& scene)
{
	// helper size (so we don't just have 8 everywhere)
	unsigned int valuesPerVector = sizeof(__m256) / sizeof(float);

	// make SoA SIMD copies of spheres (if there are any)
	if (scene.numSpheres == 0)
	{
		scene.numSpheresSIMD = 0;
	}
	else
	{
		// mathemagical way of calculating ceilf(scene.numSpheres / 8.0f)
		scene.numSpheresSIMD = (((int)scene.numSpheres) - 1) / valuesPerVector + 1;

		// allocate the correct amount of space at the correct alignment for SIMD operations
		scene.spherePosX = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numSpheresSIMD, 32);
		scene.spherePosY = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numSpheresSIMD, 32);
		scene.spherePosZ = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numSpheresSIMD, 32);
		scene.sphereSize = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numSpheresSIMD, 32);
		scene.sphereMaterialId = (__m256i*) _aligned_malloc(sizeof(__m256i) * scene.numSpheresSIMD, 32);

		// initialise SoA structures
		for (unsigned int i = 0; i < scene.numSpheresSIMD * valuesPerVector; ++i)
		{
			// don't let the source index extend out of the AoS array
			// i.e. copy the last value into the extra array slots when numSpheres isn't exactly divisible by 8
			// pretty lazy way to fix this, but it works
			int sourceIndex = i < scene.numSpheres ? i : scene.numSpheres - 1;

			scene.spherePosX[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.sphereContainer[sourceIndex].pos.x;
			scene.spherePosY[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.sphereContainer[sourceIndex].pos.y;
			scene.spherePosZ[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.sphereContainer[sourceIndex].pos.z;
			scene.sphereSize[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.sphereContainer[sourceIndex].size;
			scene.sphereMaterialId[i / valuesPerVector].m256i_i32[i % valuesPerVector] = scene.sphereContainer[sourceIndex].materialId;
		}
	}

	// make SoA SIMD copies of boxes (if there are any)
	if (scene.numBoxes == 0)
	{
		scene.numBoxesSIMD = 0;
	}
	else
	{
		// mathemagical way of calculating ceilf(scene.numBoxes / 8.0f)
		scene.numBoxesSIMD = (((int)scene.numBoxes) - 1) / valuesPerVector + 1;

		// allocate the correct amount of space at the correct alignment for SIMD operations
		scene.boxPos1X = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxPos1Y = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxPos1Z = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxPos2X = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxPos2Y = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxPos2Z = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numBoxesSIMD, 32);
		scene.boxMaterialId = (__m256i*) _aligned_malloc(sizeof(__m256i) * scene.numBoxesSIMD, 32);

		// initialise SoA structures
		for (unsigned int i = 0; i < scene.numBoxesSIMD * valuesPerVector; ++i)
		{
			// don't let the source index extend out of the AoS array
			// i.e. copy the last value into the extra array slots when numBoxes isn't exactly divisible by 8
			// pretty lazy way to fix this, but it works
			int sourceIndex = i < scene.numBoxes ? i : scene.numBoxes - 1;

			scene.boxPos1X[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p1.x;
			scene.boxPos1Y[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p1.y;
			scene.boxPos1Z[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p1.z;
			scene.boxPos2X[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p2.x;
			scene.boxPos2Y[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p2.y;
			scene.boxPos2Z[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.boxContainer[sourceIndex].p2.z;
			scene.boxMaterialId[i / valuesPerVector].m256i_i32[i % valuesPerVector] = scene.boxContainer[sourceIndex].materialId;
		}
	}
	
	// make SoA SIMD copies of lights (if there are any)
	if (scene.numLights == 0)
	{
		scene.numLightsSIMD = 0;
	}
	else
	{
		// mathemagical way of calculating ceilf(scene.numTriangles / 8.0f)
		scene.numLightsSIMD = (((int)scene.numLights) - 1) / valuesPerVector + 1;

		// allocate the correct amount of space at the correct alignment for SIMD operations
		scene.lightPosX = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);
		scene.lightPosY = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);
		scene.lightPosZ = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);

		scene.lightIntensityR = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);
		scene.lightIntensityG = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);
		scene.lightIntensityB = (__m256*) _aligned_malloc(sizeof(__m256) * scene.numLightsSIMD, 32);

		// initialise SoA structures
		for (unsigned int i = 0; i < scene.numLightsSIMD * valuesPerVector; ++i)
		{
			// don't let the source index extend out of the AoS array
			// copy the last value into the extra array slots when numLights isn't exactly divisible by 8
			int sourceIndex = i < scene.numLights ? i : scene.numLights - 1;

			scene.lightPosX[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].pos.x;
			scene.lightPosY[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].pos.y;
			scene.lightPosZ[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].pos.z;
			scene.lightIntensityR[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].intensity.red;
			scene.lightIntensityG[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].intensity.green;
			scene.lightIntensityB[i / valuesPerVector].m256_f32[i % valuesPerVector] = scene.lightContainer[sourceIndex].intensity.blue;
		}
	}
}


// read command line arguments, render, and write out BMP file
int main(int argc, char* argv[])
{
	int width = 1024;
	int height = 1024;
	int samples = 1;

	// rendering options
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	unsigned int threads = sysinfo.dwNumberOfProcessors;
	bool colourise = false;
	unsigned int blockSize = 16;
	int times = 1;
	bool testMode = false;

	// default input / output filenames
	const char* inputFilename = "Scenes/cornell.txt";

	char outputFilenameBuffer[1000];
	char* outputFilename = outputFilenameBuffer;

	// do stuff with command line args
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-size") == 0)
		{
			width = atoi(argv[++i]);
			height = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-samples") == 0)
		{
			samples = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-input") == 0)
		{
			inputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-output") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(argv[i], "-runs") == 0)
		{
			times = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-threads") == 0)
		{
			threads = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-colourise") == 0)
		{
			colourise = true;
		}
		else if (strcmp(argv[i], "-blockSize") == 0)
		{
			blockSize = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-testMode") == 0)
		{
			testMode = true;
		}
		else
		{
			fprintf(stderr, "unknown argument: %s\n", argv[i]);
		}
	}

	// nasty (and fragile) kludge to make an ok-ish default output filename (can be overriden with "-output" command line option)
	sprintf(outputFilenameBuffer, "Outputs/%s_%dx%dx%d_%s.bmp", (strrchr(inputFilename, '/') + 1), width, height, samples, (strrchr(argv[0], '\\') + 1));

	// read scene file
	Scene scene;
	if (!init(inputFilename, scene))
	{
		fprintf(stderr, "Failure when reading the Scene file.\n");
		return -1;
	}

	// do the SoA things
	simdifySceneContainers(scene);

	// total time taken to render all runs (used to calculate average)
	int totalTime = 0;
	int samplesRendered = 0;
	for (int i = 0; i < times; i++)
	{
		Timer timer;																						// create timer
		samplesRendered = render(threads, &scene, width, height, samples, blockSize, testMode, colourise);	// raytrace scene
		timer.end();																						// record end time
		totalTime += timer.getMilliseconds();																// record total time taken
	}

	// output timing information (times run and average)
	printf("rendered %d samples, average time taken (%d run(s)): %ums\n", samplesRendered, times, totalTime / times);

	// output BMP file
	write_bmp(outputFilename, buffer, width, height, width);
}
