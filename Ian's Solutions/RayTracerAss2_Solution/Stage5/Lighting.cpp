/*  The following code is a VERY heavily modified from code originally sourced from:
Ray tracing tutorial of http://www.codermind.com/articles/Raytracer-in-C++-Introduction-What-is-ray-tracing.html
It is free to use for educational purpose and cannot be redistributed outside of the tutorial pages. */

#include "Lighting.h"
#include "Colour.h"
#include "Intersection.h"
#include "Texturing.h"
#include "PrimitivesSIMD.h"
#include "MathSIMD.h"

// horizontally add all the numbers in a vector
__forceinline float horizAdd(__m256 x)
{
	__m256 temp = _mm256_hadd_ps(x, x);
	__m256 temp2 = _mm256_hadd_ps(temp, temp);
	__m256i perm = _mm256_set_epi32(0, 5, 0, 5, 0, 5, 0, 5);
	__m256 temp3 = _mm256_permutevar8x32_ps(temp2, perm);
	__m256 temp4 = _mm256_hadd_ps(temp3, temp3);

	return temp4.m256_f32[0];
}


// test to see if light ray collides with any of the scene's objects
// short-circuits when first intersection discovered, because no matter what the object will be in shadow
bool isInShadow(const Scene* scene, const Ray* lightRay, const float lightDist)
{
	float t = lightDist;

	// search for sphere collision
	if (isSphereIntersected(scene, lightRay, t)) return true;

	// search for triangle collision
	if (isBoxIntersected(scene, lightRay, t)) return true;

	// not in shadow
	return false;
}


// combined applyDiffuse function that I used as a model for SIMD version
// apply diffuse lighting with respect to material's colouring
//Colour applyDiffuse(const Ray* lightRay, const Light* currentLight, const Intersection* intersect)
//{
//	Colour output;
//
//	Point p = (intersect->pos - intersect->material->offset) / intersect->material->size;
//
//	int which = 1;
//
//	if (intersect->material->type == Material::GOURAUD)
//	{
//		which = 1;
//	}
//	else if (intersect->material->type == Material::CHECKERBOARD)
//	{
//		which = (int(floorf(p.x)) + int(floorf(p.y)) + int(floorf(p.z))) & 1;
//	}
//	else
//	{
//		if (intersect->material->type == Material::WOOD)
//		{
//			p = { p.x * cosf(p.y * 0.996f) * sinf(p.z * 1.023f), cosf(p.x) * p.y * sinf(p.z * 1.211f), cosf(p.x * 1.473f) * cosf(p.y * 0.795f) * p.z };
//		}
//		which = int(floorf(sqrtf(p.x*p.x + p.y*p.y + p.z*p.z))) & 1;
//	}
//
//	output = (which ? intersect->material->diffuse : intersect->material->diffuse2);
//
//	float lambert = lightRay->dir * intersect->normal;
//
//	return lambert * currentLight->intensity * output;
//}


Colour applyDiffuse1Scalar(const Intersection* intersect)
{
	Colour output;

	Point p = (intersect->pos - intersect->material->offset) / intersect->material->size;

	int which = 1;

	if (intersect->material->type == Material::GOURAUD)
	{
		which = 1;
	}
	else if (intersect->material->type == Material::CHECKERBOARD)
	{
		which = (int(floorf(p.x)) + int(floorf(p.y)) + int(floorf(p.z))) & 1;
	}
	else
	{
		if (intersect->material->type == Material::WOOD)
		{
			p = { p.x * cosf(p.y * 0.996f) * sinf(p.z * 1.023f), cosf(p.x) * p.y * sinf(p.z * 1.211f), cosf(p.x * 1.473f) * cosf(p.y * 0.795f) * p.z };
		}
		which = int(floorf(sqrtf(p.x*p.x + p.y*p.y + p.z*p.z))) & 1;
	}

	output = (which ? intersect->material->diffuse : intersect->material->diffuse2);

	return output;
}


// this is unused as it turned out to be slower than the previous function
// apply diffuse lighting with respect to material's colouring
__forceinline Colour8 applyDiffuse1(
	//const Vector8 lightRayStart, const Vector8 lightRayDir,
	//const Vector8 currentLightPos, const Colour8 currentLightIntensity,
	const Vector8 intersectPos, const __m256i intersectMaterialType, const Vector8 intersectMaterialOffset, const __m256 intersectMaterialSize, const Colour8 intersectMaterialDiffuse, const Colour8 intersectMaterialDiffuse2)
{
	Colour8 output;
	const __m256i ones = _mm256_set1_epi32(1);
	__m256i isGourard = _mm256_cmpeq_epi32(intersectMaterialType, _mm256_set1_epi32(Material::GOURAUD));
	__m256i isChecker = _mm256_cmpeq_epi32(intersectMaterialType, _mm256_set1_epi32(Material::CHECKERBOARD));
	__m256i isCircles = _mm256_cmpeq_epi32(intersectMaterialType, _mm256_set1_epi32(Material::CIRCLES));
	__m256i isWood = _mm256_cmpeq_epi32(intersectMaterialType, _mm256_set1_epi32(Material::WOOD));

	// offsetted / scaled texture coordinate
	Vector8 p = (intersectPos - intersectMaterialOffset) / intersectMaterialSize;

	// calculate gourard shading
	__m256i gourard = _mm256_set1_epi32(0xFFFFFFFF);

	// calculate checkerboard texture
	__m256i checker = _mm256_cmpeq_epi32(ones, ones & (_mm256_cvtps_epi32(_mm256_floor_ps(p.xs) + _mm256_floor_ps(p.ys) + _mm256_floor_ps(p.zs))));

	// calculate wood and circles texture
	// calculate squiggled coordinates for calculating wood texture
	Vector8 pSquiggled;
	pSquiggled.xs = p.xs * cos256_ps(p.ys * _mm256_set1_ps(0.996f)) * sin256_ps(p.zs * _mm256_set1_ps(1.023f));
	pSquiggled.ys = cos256_ps(p.xs) * p.ys * sin256_ps(p.zs * _mm256_set1_ps(1.211f));
	pSquiggled.zs = cos256_ps(p.xs * _mm256_set1_ps(1.473f)) * cos256_ps(p.ys * _mm256_set1_ps(0.795f)) * p.zs;

	// choose coordinates as normal (circles) or squiggled (wood)
	p = select(_mm256_castsi256_ps(isWood), pSquiggled, p);

	__m256i circlesOrWood = _mm256_cmpeq_epi32(ones, ones & _mm256_cvtps_epi32(_mm256_floor_ps(_mm256_sqrt_ps(dot(p, p)))));

	// choose correct texture value
	__m256i which = (gourard & isGourard) | (checker & isChecker) | (circlesOrWood & (isCircles | isWood));
	output = select(_mm256_castsi256_ps(which), intersectMaterialDiffuse, intersectMaterialDiffuse2);

	// do the rest of the lighting calculation
	//__m256 lambert = dot(lightRayDir, intersectNormal);
	//output = lambert * currentLightIntensity * output;

	return output;
}

__forceinline Colour8 applyDiffuse2(const Vector8 lightRayStart, const Vector8 lightRayDir,
	const Vector8 currentLightPos, const Colour8 currentLightIntensity,
	const Vector8 intersectNormal, const Colour8 diffuseColour)
{
	Colour8 output;

	// do the rest of the lighting calculation
	__m256 lambert = dot(lightRayDir, intersectNormal);
	output = lambert * currentLightIntensity * diffuseColour;

	return output;
}

// Blinn 
// The direction of Blinn is exactly at mid point of the light ray and the view ray. 
// We compute the Blinn vector and then we normalize it then we compute the coeficient of blinn
// which is the specular contribution of the current light.
__forceinline Colour8 applySpecular(const Vector8 lightRayStart, const Vector8 lightRayDir,
	const Vector8 currentLightPos, const Colour8 currentLightIntensity, const __m256 fLightProjection,
	const Vector8 viewRayStart, const Vector8 viewRayDir,
	const __m256 intersectViewProjection, const __m256 intersectMaterialPower, const Colour8 intersectMaterialSpecular)
{
	Vector8 blinnDir = lightRayDir - viewRayDir;
	__m256 blinn = _mm256_rsqrt_ps(dot(blinnDir, blinnDir));
	blinn = blinn * _mm256_max_ps(fLightProjection - intersectViewProjection, _mm256_setzero_ps());
	blinn = pow256_ps(blinn, intersectMaterialPower);

	return blinn * intersectMaterialSpecular * currentLightIntensity;
}


// apply diffuse and specular lighting contributions for all lights in scene taking shadowing into account
Colour applyLighting(const Scene* scene, const Ray* viewRay, const Intersection* intersect)
{
	// colour to return (starts as black)
	Colour8 output;

	// constants
	__m256 ones = _mm256_set1_ps(1.0f);
	__m256i eights = _mm256_set1_epi32(8);

	// splatted view ray
	Vector8 viewRayStart(viewRay->start.x, viewRay->start.y, viewRay->start.z);
	Vector8 viewRayDir(viewRay->dir.x, viewRay->dir.y, viewRay->dir.z);

	// same starting point for each light ray
	Ray lightRay = { intersect->pos };
	Vector8 lightRayStart(lightRay.start.x, lightRay.start.y, lightRay.start.z);

	// splatted intersection values
	Vector8 intersectPos(intersect->pos.x, intersect->pos.y, intersect->pos.z);
	Vector8 intersectNormal(intersect->normal.x, intersect->normal.y, intersect->normal.z);
	__m256 intersectViewProjection = _mm256_set1_ps(intersect->viewProjection);
	Colour8 intersectMaterialSpecular(intersect->material->specular.red, intersect->material->specular.green, intersect->material->specular.blue);
	__m256 intersectMaterialPower = _mm256_set1_ps(intersect->material->power);

	// current index for each lane
	__m256i indexes = _mm256_setr_epi32(0, 1, 2, 3, 4, 5, 6, 7);

	// splatted intersection values needed for a SIMD applyDiffuse1
	//__m256 intersectMaterialSize = _mm256_set1_ps(intersect->material->size);
	//__m256i intersectMaterialType = _mm256_set1_epi32(intersect->material->type);
	//Vector8 intersectMaterialOffset(intersect->material->offset.x, intersect->material->offset.y, intersect->material->offset.z);
	//Colour8 intersectMaterialDiffuse(intersect->material->diffuse.red, intersect->material->diffuse.green, intersect->material->diffuse.blue);
	//Colour8 intersectMaterialDiffuse2(intersect->material->diffuse2.red, intersect->material->diffuse2.green, intersect->material->diffuse2.blue);
	//// SIMD version of first part of diffuse calculation (slower)
	//Colour8 diffuse = applyDiffuse1(intersectPos, intersectMaterialType, intersectMaterialOffset, intersectMaterialSize, intersectMaterialDiffuse, intersectMaterialDiffuse2);

	// calculate most of the diffuse value in advance
	// doing this in scalar with a splat, turned out faster than splatting, then doing a SIMD calc
	// uncomment the above (and comment out this) to try the other way
	Colour diffuseScalar = applyDiffuse1Scalar(intersect);
	Colour8 diffuse(diffuseScalar.red, diffuseScalar.green, diffuseScalar.blue);

	// loop through all the lights
	for (unsigned int j = 0; j < scene->numLightsSIMD; ++j)
	{
		Vector8 currentLightPos(scene->lightPosX[j], scene->lightPosY[j], scene->lightPosZ[j]);
		Colour8 currentLightIntensity(scene->lightIntensityR[j], scene->lightIntensityG[j], scene->lightIntensityB[j]);

		// light ray direction need to equal the normalised vector in the direction of the current light
		// as we need to reuse all the intermediate components for other calculations, 
		// we calculate the normalised vector by hand instead of using the normalise function
		Vector8 lightRayDir = (currentLightPos - intersectPos);
		__m256 angleBetweenLightAndNormal = dot(lightRayDir, intersectNormal);

		// distance to light from intersection point (and it's inverse)
		__m256 lightDist = _mm256_sqrt_ps(dot(lightRayDir, lightRayDir));
		__m256 invLightDist = ones / lightDist;

		// light ray projection
		__m256 lightProjection = invLightDist * angleBetweenLightAndNormal;

		// normalise the light direction
		lightRayDir.xs = lightRayDir.xs * invLightDist;
		lightRayDir.ys = lightRayDir.ys * invLightDist;
		lightRayDir.zs = lightRayDir.zs * invLightDist;

		// calculate specular lighting for these lights
		Colour8 spec = applySpecular(
			lightRayStart, lightRayDir,
			currentLightPos, currentLightIntensity, lightProjection,
			viewRayStart, viewRayDir,
			intersectViewProjection, intersectMaterialPower, intersectMaterialSpecular);

		// calculate diffuse lighting for these lights
		Colour8 diff = applyDiffuse2(lightRayStart, lightRayDir,
			currentLightPos, currentLightIntensity,
			intersectNormal, diffuse);

		// keep the light data if normal pointing the correct way
		__m256 keep = _mm256_cmp_ps(angleBetweenLightAndNormal, _mm256_setzero_ps(), _CMP_GT_OQ);

		// work out if current loop counter is valid (i.e. if this is a real light)
		__m256i validIndexes = _mm256_cmpgt_epi32(_mm256_set1_epi32(scene->numLights), indexes);

		// keep value only if the index is valid
		keep = keep & _mm256_castsi256_ps(validIndexes);

		// calculate which lights are in shadow (a scalar loop, otherwise we'd have to rewrite isInShadow again)
		__m256i notInShadowMask = _mm256_set1_epi32(0xFFFFFFFF);
		for (unsigned int simd = 0; simd < 8; simd++)
		{
			// bail out in cases where there's not enough lights
			// also while we are here don't do the shadow check if we aren't keeping this value anyways
			if (!keep.m256_f32[simd]) continue;

			// reconstitute a ray
			lightRay.dir.x = lightRayDir.xs.m256_f32[simd];
			lightRay.dir.y = lightRayDir.ys.m256_f32[simd];
			lightRay.dir.z = lightRayDir.zs.m256_f32[simd];

			notInShadowMask.m256i_u32[simd] = isInShadow(scene, &lightRay, lightDist.m256_f32[simd]) ? 0 : 0xFFFFFFFF;
		}

		// keep value only if not in shadow
		keep = keep & _mm256_castsi256_ps(notInShadowMask);

		// add this light data only the total output
		output = output + select(keep, diff + spec, Colour8());

		// increase the indexes
		indexes = _mm256_add_epi32(eights, indexes);
	}

	// produce a single colour
	return Colour(horizAdd(output.reds), horizAdd(output.greens), horizAdd(output.blues));
}
