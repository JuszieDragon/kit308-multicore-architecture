// SIMD versions of (some of the) helper functions from primitives.h

#ifndef __PRIMITIVES_SIMD_H
#define __PRIMITIVES_SIMD_H

#include <immintrin.h>
//#include "MathSIMD.h"

__m256 exp256_ps(__m256 x);

// a bunch of operators to replace nasty instrinsics
__forceinline __m256 operator - (const __m256 x, const __m256 y) { return _mm256_sub_ps(x, y); }
__forceinline __m256 operator + (const __m256 x, const __m256 y) { return _mm256_add_ps(x, y); }
__forceinline __m256 operator * (const __m256 x, const __m256 y) { return _mm256_mul_ps(x, y); }
__forceinline __m256 operator / (const __m256 x, const __m256 y) { return _mm256_div_ps(x, y); }
__forceinline __m256 operator < (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_LT_OQ); }
__forceinline __m256 operator > (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_GT_OQ); }
__forceinline __m256 operator <= (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_LE_OQ); }
__forceinline __m256 operator >= (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_GE_OQ); }
__forceinline __m256 operator & (const __m256 x, const __m256 y) { return _mm256_and_ps(x, y); }
__forceinline __m256 operator | (const __m256 x, const __m256 y) { return _mm256_or_ps(x, y); }
__forceinline __m256 operator == (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_EQ_OQ); }
__forceinline __m256 operator != (const __m256 x, const __m256 y) { return _mm256_cmp_ps(x, y, _CMP_NEQ_OQ); }


__forceinline __m256i operator & (const __m256i x, const __m256i y) { return _mm256_and_si256(x, y); }
__forceinline __m256i operator | (const __m256i x, const __m256i y) { return _mm256_or_si256(x, y); }

__forceinline __m256i operator + (const __m256i x, const __m256i y) { return _mm256_add_epi32(x, y); }
__forceinline __m256i operator * (const __m256i x, const __m256i y) { return _mm256_mul_epi32(x, y); }


// Represent 8 vectors in one struct
struct Vector8
{
	__m256 xs, ys, zs;

	__forceinline Vector8(float x, float y, float z)
	{
		xs = _mm256_set1_ps(x);
		ys = _mm256_set1_ps(y);
		zs = _mm256_set1_ps(z);
	}

	__forceinline Vector8(__m256 xsIn, __m256 ysIn, __m256 zsIn)
	{
		xs = xsIn;
		ys = ysIn;
		zs = zsIn;
	}

	__forceinline Vector8()
	{
		xs = ys = zs = _mm256_setzero_ps();
	}
};


// helper operators / functions for Vector8s
__forceinline Vector8 operator - (const Vector8& v1, const Vector8& v2) { return { v1.xs - v2.xs, v1.ys - v2.ys, v1.zs - v2.zs }; }
__forceinline Vector8 operator + (const Vector8& v1, const Vector8& v2) { return { v1.xs + v2.xs, v1.ys + v2.ys, v1.zs + v2.zs }; }
__forceinline Vector8 operator * (const Vector8& v1, const Vector8& v2) { return { v1.xs * v2.xs, v1.ys * v2.ys, v1.zs * v2.zs }; }
__forceinline Vector8 operator / (const Vector8& v1, const Vector8& v2) { return { v1.xs / v2.xs, v1.ys / v2.ys, v1.zs / v2.zs }; }

__forceinline Vector8 operator / (const Vector8& v1, const __m256 x) { return { v1.xs / x, v1.ys / x, v1.zs / x }; }

__forceinline Vector8 cross(const Vector8& v1, const Vector8& v2)
{
	return { v1.ys * v2.zs - v1.zs * v2.ys, v1.zs * v2.xs - v1.xs * v2.zs, v1.xs * v2.ys - v1.ys * v2.xs };
}


__forceinline __m256 dot(const Vector8& v1, const Vector8& v2)
{
	return v1.xs * v2.xs + v1.ys * v2.ys + v1.zs * v2.zs;
}

__forceinline Vector8 normalise(const Vector8& x)
{
	return x / _mm256_sqrt_ps(dot(x, x)); //TODO: why no _mm256_invsqrt_ps??
}

__forceinline __m256 select(__m256 cond, __m256 ifTrue, __m256 ifFalse)
{
	return _mm256_or_ps(_mm256_and_ps(cond, ifTrue), _mm256_andnot_ps(cond, ifFalse));
}

__forceinline __m256i select(__m256i cond, __m256i ifTrue, __m256i ifFalse)
{
	return _mm256_or_si256(_mm256_and_si256(cond, ifTrue), _mm256_andnot_si256(cond, ifFalse));
}

__forceinline Vector8 select(__m256 cond, const Vector8& v1, const Vector8& v2)
{
	return Vector8(select(cond, v1.xs, v2.xs), select(cond, v1.ys, v2.ys), select(cond, v1.zs, v2.zs));
}

struct Colour8
{
	__m256 reds, greens, blues;

	__forceinline Colour8(float x, float y, float z)
	{
		reds = _mm256_set1_ps(x);
		greens = _mm256_set1_ps(y);
		blues = _mm256_set1_ps(z);
	}

	__forceinline Colour8(__m256 xsIn, __m256 ysIn, __m256 zsIn)
	{
		reds = xsIn;
		greens = ysIn;
		blues = zsIn;
	}

	__forceinline Colour8()
	{
		reds = greens = blues = _mm256_setzero_ps();
	}

	__forceinline void colourise(unsigned int colourMask)
	{
		reds = reds * ((colourMask & 4) ? _mm256_set1_ps(1.5f) : _mm256_set1_ps(0.75f));
		greens = greens * ((colourMask & 2) ? _mm256_set1_ps(1.5f) : _mm256_set1_ps(0.75f));
		blues = blues * ((colourMask & 1) ? _mm256_set1_ps(1.5f) : _mm256_set1_ps(0.75f));
	}

	__forceinline __m256i convertToPixel(float exposure)
	{
		const __m256 ones = _mm256_set1_ps(1.0f);
		const __m256 twoFiddyFive = _mm256_set1_ps(255.0f);
		__m256 exposures = _mm256_set1_ps(exposure);

		__m256i rs = _mm256_cvtps_epi32(_mm256_min_ps(ones - exp256_ps(reds * exposures), ones) * twoFiddyFive);
		__m256i gs = _mm256_cvtps_epi32(_mm256_min_ps(ones - exp256_ps(greens * exposures), ones) * twoFiddyFive);
		__m256i bs = _mm256_cvtps_epi32(_mm256_min_ps(ones - exp256_ps(blues * exposures), ones) * twoFiddyFive);

		bs = _mm256_sll_epi32(bs, _mm_setr_epi32(16, 0, 0, 0));
		gs = _mm256_sll_epi32(gs, _mm_setr_epi32(8, 0, 0, 0));

		return rs | gs | bs;
	}

	// convert colour to pixel (in 0x00BBGGRR format)
	__forceinline __m256i convertToPixel()
	{
		const __m256 ones = _mm256_set1_ps(1.0f);
		const __m256 twoFiddyFive = _mm256_set1_ps(255.0f);

		__m256i rs = _mm256_cvtps_epi32(_mm256_min_ps(reds, ones) * twoFiddyFive);
		__m256i gs = _mm256_cvtps_epi32(_mm256_min_ps(greens, ones) * twoFiddyFive);
		__m256i bs = _mm256_cvtps_epi32(_mm256_min_ps(blues, ones) * twoFiddyFive);

		bs = _mm256_sll_epi32(bs, _mm_setr_epi32(16, 0, 0, 0));
		gs = _mm256_sll_epi32(gs, _mm_setr_epi32(8, 0, 0, 0));

		return rs | gs | bs;
	}
};

/*
__forceinline Colour8 operator - (const Colour8& v1, const Colour8& v2) { return { v1.xs - v2.xs, v1.ys - v2.ys, v1.zs - v2.zs }; }
__forceinline Colour8 operator + (const Colour8& v1, const Colour8& v2) { return { v1.xs + v2.xs, v1.ys + v2.ys, v1.zs + v2.zs }; }*/

__forceinline Colour8 operator - (const Colour8& v1, const Colour8& v2) { return { v1.reds - v2.reds, v1.greens - v2.greens, v1.blues - v2.blues }; }
__forceinline Colour8 operator + (const Colour8& v1, const Colour8& v2) { return { v1.reds + v2.reds, v1.greens + v2.greens, v1.blues + v2.blues }; }
__forceinline Colour8 operator * (const Colour8& v1, const Colour8& v2) { return { v1.reds * v2.reds, v1.greens * v2.greens, v1.blues * v2.blues }; }
__forceinline Colour8 operator / (const Colour8& v1, const Colour8& v2) { return { v1.reds / v2.reds, v1.greens / v2.greens, v1.blues / v2.blues }; }

__forceinline Colour8 operator * (const __m256 x, const Colour8& c) { return { x * c.reds, x * c.greens, x * c.blues }; }
__forceinline Colour8 operator / (const Colour8& c, const __m256 x) { return { c.reds / x, c.greens / x,  c.blues / x }; }

__forceinline Colour8 select(__m256 cond, const Colour8& ifTrue, const Colour8& ifFalse)
{
	return Colour8(select(cond, ifTrue.reds, ifFalse.reds), select(cond, ifTrue.greens, ifFalse.greens), select(cond, ifTrue.blues, ifFalse.blues));
}

struct Ray8
{
	Vector8 pos;
	Vector8 dir;
};

#endif

