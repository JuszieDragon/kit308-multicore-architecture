#include <stdio.h>
#include <time.h>

// number of particles
#define N 1024*1024

// number of times to time each test
#define TIMES 1024

#define PADDING 4
// particle positions stored as an array of structures (AoS)
struct Position
{
	float x;
	//char padding[PADDING]; // artificially force seperation of values
	float y;
	//char padding2[PADDING]; // artificially force seperation of values
	float z;
	//char padding3[PADDING]; // artificially force seperation of values
};
//Position particle_positions[N];

struct Positions
{
	float x[N];
	float y[N];
	float z[N];
};

Positions particle_positions;

// move a particle position by a specified offset
/*void Offset(Positions& p, const Positions& offset) {
	p.x += offset.x;
	p.y += offset.y;
	p.z += offset.z;
}*/

int main(void)
{
	// timing variables
	clock_t start, finish;

	// amount to move each particle by
	Position offset;
	offset.x = 10;
	offset.y = 5;
	offset.z = 7;

	// size of particle positions list
	printf("sizeof(particle_positions) = %zd\n", sizeof(particle_positions));

	start = clock();

	// loop TIMES times to ensure proper timing
	for (int times = 0; times < TIMES; times++)
	{
		// loop through whole particle position list and move each one
		for (int i = 0; i < N; i += 16)
		{
			particle_positions.x[i] += offset.x;
			particle_positions.x[i + 1] += offset.x;
			particle_positions.x[i + 2] += offset.x;
			particle_positions.x[i + 3] += offset.x;
			particle_positions.x[i + 4] += offset.x;
			particle_positions.x[i + 5] += offset.x;
			particle_positions.x[i + 6] += offset.x;
			particle_positions.x[i + 7] += offset.x;
			particle_positions.x[i + 8] += offset.x;
			particle_positions.x[i + 9] += offset.x;
			particle_positions.x[i + 10] += offset.x;
			particle_positions.x[i + 11] += offset.x;
			particle_positions.x[i + 12] += offset.x;
			particle_positions.x[i + 13] += offset.x;
			particle_positions.x[i + 14] += offset.x;
			particle_positions.x[i + 15] += offset.x;
			particle_positions.y[i] += offset.y;
			particle_positions.y[i + 1] += offset.y;
			particle_positions.y[i + 2] += offset.y;
			particle_positions.y[i + 3] += offset.y;
			particle_positions.y[i + 4] += offset.y;
			particle_positions.y[i + 5] += offset.y;
			particle_positions.y[i + 6] += offset.y;
			particle_positions.y[i + 7] += offset.y;
			particle_positions.y[i + 8] += offset.y;
			particle_positions.y[i + 9] += offset.y;
			particle_positions.y[i + 10] += offset.y;
			particle_positions.y[i + 11] += offset.y;
			particle_positions.y[i + 12] += offset.y;
			particle_positions.y[i + 13] += offset.y;
			particle_positions.y[i + 14] += offset.y;
			particle_positions.y[i + 15] += offset.y;
			particle_positions.z[i] += offset.z;
			particle_positions.z[i + 1] += offset.z;
			particle_positions.z[i + 2] += offset.z;
			particle_positions.z[i + 3] += offset.z;
			particle_positions.z[i + 4] += offset.z;
			particle_positions.z[i + 5] += offset.z;
			particle_positions.z[i + 6] += offset.z;
			particle_positions.z[i + 7] += offset.z;
			particle_positions.z[i + 8] += offset.z;
			particle_positions.z[i + 9] += offset.z;
			particle_positions.z[i + 10] += offset.z;
			particle_positions.z[i + 11] += offset.z;
			particle_positions.z[i + 12] += offset.z;
			particle_positions.z[i + 13] += offset.z;
			particle_positions.z[i + 14] += offset.z;
			particle_positions.z[i + 15] += offset.z;
		}
	}

	finish = clock();

	// output time taken for this run
	printf("time taken %ums\n", finish - start);

	// output a selection of the particle positions as rough check
	for (int i = 0; i < N; i += N / 7)
	{
		printf("%14d @ (%f, %f, %f)\n", i, particle_positions.x[i],
			particle_positions.y[i], particle_positions.z[i]);
	}

	return 0;
}
