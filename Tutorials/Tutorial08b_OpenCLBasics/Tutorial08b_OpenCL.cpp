#include <stdio.h>
#include <stdlib.h>
#pragma warning(disable: 4996) // so clCreateCommandQueue still works
#include <CL/cl.h>
#include "LoadCL.h"

// size of the buffer
const unsigned int BUFFER_SIZE_SQRT = 1000;
const unsigned int BUFFER_SIZE = BUFFER_SIZE_SQRT * BUFFER_SIZE_SQRT;

// buffer
float bufferIn1[BUFFER_SIZE];
float bufferIn2[BUFFER_SIZE];
float bufferOut[BUFFER_SIZE];

int main(int argc, char **argv)
{
	// set a random seed because
	srand(17372);

	// intialise the buffer with some random stuff and output some elements
	for (int i = 0; i < BUFFER_SIZE; i++) bufferIn1[i] = rand() / 1000.0f;
	for (int i = 0; i < BUFFER_SIZE; i++) bufferIn2[i] = rand() / 1000.0f;
	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", bufferIn1[i]);
	printf("\n");
	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", bufferIn2[i]);
	printf("\n");

	//TODO: comment out these loops
	// perform a pretty trivial calculation and output some array elements
	/*for (int i = 0; i < BUFFER_SIZE_SQRT; i++) {
		for (int j = 0; j < BUFFER_SIZE_SQRT; j++) {
			bufferOut[i * BUFFER_SIZE_SQRT + j] = bufferIn1[i * BUFFER_SIZE_SQRT + j] + bufferIn2[i * BUFFER_SIZE_SQRT + j];
		}
	}
	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", bufferOut[i]);
	printf("\n");*/

	//TODO: all the OpenCL things
	cl_int err;

	cl_platform_id platform;

	err = clGetPlatformIDs(1, &platform, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetPlatformIDs. Error code: %d\n", err);
		exit(1);
	}

	cl_device_id deviceId;

	cl_uint numDevices = 1;

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, &deviceId, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetDeviceIDs. Error code: %d\n", err);
		exit(1);
	}

	cl_context context;

	context = clCreateContext(NULL, numDevices, &deviceId, NULL, NULL, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateContext. Error code: %d\n", err);
		exit(1);
	}

	cl_command_queue commandQueue;

	//commandQueue = clCreateCommandQueueWithProperties(context, deviceId, NULL, &err);
	commandQueue = clCreateCommandQueue(context, deviceId, NULL, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateCommandQueue. Error code: %d\n", err);
		exit(1);
	}

	cl_program clProgram;

	clProgram = clLoadSource(context, "Add2D.cl", &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clLoadSource. Error code: %d\n", err);
		exit(1);
	}

	err = clBuildProgram(clProgram, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		char* program_log;
		size_t log_size;

		clGetProgramBuildInfo(clProgram, deviceId, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		program_log = (char*)malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(clProgram, deviceId, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
		printf("%s\n", program_log);
		free(program_log);

		printf("build program failed %d\n", err);
		exit(1);
	}

	cl_kernel clKernal;

	clKernal = clCreateKernel(clProgram, "func", &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateKernel. Error code: %d\n", err);
		exit(1);
	}

	cl_mem clBufferOut;

	clBufferOut = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(bufferOut), bufferOut, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateBuffer1. Error code: %d\n", err);
		exit(1);
	}

	
	cl_mem clBufferIn1;

	clBufferIn1 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(bufferIn1), bufferIn1, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateBuffer1. Error code: %d\n", err);
		exit(1);
	}

	cl_mem clBufferIn2;

	clBufferIn2 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(bufferIn2), bufferIn2, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateBuffer2. Error code: %d\n", err);
		exit(1);
	}

	err = clSetKernelArg(clKernal, 0, sizeof(clBufferOut), &clBufferOut);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clSetKernelArg. Error code: %d\n", err);
		exit(1);
	}

	err = clSetKernelArg(clKernal, 1, sizeof(clBufferIn1), &clBufferIn1);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clSetKernelArg. Error code: %d\n", err);
		exit(1);
	}
	
	err = clSetKernelArg(clKernal, 2, sizeof(clBufferIn2), &clBufferIn2);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clSetKernelArg. Error code: %d\n", err);
		exit(1);
	}

	const size_t workOffset[] = { 0, 0 };
	const size_t workSize[] = {BUFFER_SIZE_SQRT, BUFFER_SIZE_SQRT};

	err = clEnqueueNDRangeKernel(commandQueue, clKernal, 2, workOffset, workSize, NULL, 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clEnqueueNDRangeKernel. Error code: %d\n", err);
		exit(1);
	}

	err = clEnqueueReadBuffer(commandQueue, clBufferOut, CL_TRUE, 0, sizeof(bufferOut), bufferOut, 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clEnqueueReadBuffer. Error code: %d\n", err);
		exit(1);
	}
	
	clReleaseMemObject(clBufferOut);
	clReleaseMemObject(clBufferIn1);
	clReleaseMemObject(clBufferIn2);
	clReleaseCommandQueue(commandQueue);
	clReleaseProgram(clProgram);
	clReleaseKernel(clKernal);
	clReleaseContext(context);

	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", bufferOut[i]);
	printf("\n");
}
