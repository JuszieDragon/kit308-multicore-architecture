//TODO: write an OpenCL kernel here
__kernel void func(__global float* bufferOut, __global float* bufferIn1, __global float* bufferIn2)
{
	int i = get_global_id(0);
	int j = get_global_id(1);
	int BUFFER_SIZE_SQRT = get_global_size(0);
	bufferOut[i * BUFFER_SIZE_SQRT + j] = bufferIn1[i * BUFFER_SIZE_SQRT + j] + bufferIn2[i * BUFFER_SIZE_SQRT + j];
};