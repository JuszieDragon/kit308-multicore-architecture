#include <windows.h>
#include <stdio.h>
#include <immintrin.h>
#include "Mandelbrot.h"

// convert iterations to colour information
inline unsigned int iterations2colour(unsigned int iter, unsigned int max_iter, unsigned int flags)
{
	// bound iterations to number of available colours
	iter = (iter * MAX_COLOURS / max_iter) & (MAX_COLOURS - 1);

	// convert iterations to colour scale based on flags (7 basic colour scales possible)
	return (((flags & 4) << 14) | ((flags & 2) << 7) | (flags & 1)) * iter;
}

// calculate Mandelbrot set
void mandelbrot(unsigned int threadID, unsigned int iterations, float centrex, float centrey, float scaley,
	unsigned int samples, unsigned int width, unsigned int height, unsigned int* out, unsigned int* lineCount)
{
	// calculate the x distance to render based on aspect ratio of desired image and y-scale given
	float scalex = scaley * width / height;

	// four copies of samples
	__m128i sampless = _mm_set1_epi32(samples);

	// calculate step size for x- and y-axis on the complex plane
	__m128 dxs = _mm_set1_ps(scalex / width / samples);
	__m128 dys = _mm_set1_ps(scaley / height / samples);

	// calculate top-left position on the complex plane
	__m128 startxs = _mm_set1_ps(centrex - scalex * 0.5f);
	__m128 startys = _mm_set1_ps(centrey - scaley * 0.5f);

	// potentially loop through entire image size (ie. (0,0) to (width - 1, height - 1)), doing a single row at a time
	unsigned int iy;

	while ((iy = InterlockedIncrement(lineCount)) < height)
	{
		const int SIMD_WIDTH = 4;

		for (unsigned int ix = 0; ix < width; ix += SIMD_WIDTH)
		{
			__m128i totalCalcs = _mm_set1_epi32(0);

			for (unsigned int aay = 0; aay < samples; aay++)
			{
				for (unsigned int aax = 0; aax < samples; aax++)
				{
					__m128i iters = _mm_set1_epi32(0);

					// calculate four x-locations on the complex plane to render for the current pixels
					__m128i xcoords = _mm_add_epi32(_mm_set1_epi32(ix), _mm_set_epi32(3, 2, 1, 0));
					__m128i xcoordsSamples = _mm_add_epi32(_mm_mullo_epi32(xcoords, sampless), _mm_set1_epi32(aax));
					__m128 x0s = _mm_add_ps(startxs, _mm_mul_ps(_mm_cvtepi32_ps(xcoordsSamples), dxs));

					// calculate y-location on the complex plane to render for the current pixels
					__m128i ycoords = _mm_set1_epi32(iy);
					__m128i ycoordsSamples = _mm_add_epi32(_mm_mullo_epi32(ycoords, sampless), _mm_set1_epi32(aay));
					__m128 y0s = _mm_add_ps(startys, _mm_mul_ps(_mm_cvtepi32_ps(ycoordsSamples), dys));

					// initialise complex number z represented in x and y (ie. z = (x + yi)) 
					// to be current locations in complex plane 
					__m128 xs = x0s;
					__m128 ys = y0s;

					//printf("x0: %f %f %f %f, y0: %f\n", x0s.m128_f32[0], x0s.m128_f32[1], x0s.m128_f32[2], x0s.m128_f32[3], y0s.m128_f32[0]);

					__m128i iterCounts = _mm_set1_epi32(0);
					while (iterCounts.m128i_u32[0] <= iterations)
					{
						//TODO: STEP 1: declare an appropritate SIMD vector type to replace this array of bools
						//bool stopCalcs[4];
						__m128 stopCalcs;

						
						//TODO: STEP 1: convert this loop to SIMD vector calculation using an appropriate compare intrinsic
						/*for (unsigned int simdx = 0; simdx < SIMD_WIDTH; simdx++)
						{
							stopCalcs.m128_f32[simdx] = (xs.m128_f32[simdx] * xs.m128_f32[simdx] + ys.m128_f32[simdx] * ys.m128_f32[simdx] >= (2 * 2));
						}*/

						//printf("Scalar stopCalcs: %d %d %d %d\n", stopCalcs.m128_f32[0], stopCalcs.m128_f32[1], stopCalcs.m128_f32[2], stopCalcs.m128_f32[3]);

						__m128 magnitudes = _mm_add_ps(_mm_mul_ps(xs, xs), _mm_mul_ps(ys, ys));
						stopCalcs = _mm_cmpge_ps(magnitudes, _mm_set1_ps(2.0 * 2.0));
						__m128i stopCalcsi = _mm_castps_si128(stopCalcs);

						//printf("SIMD stopCalcs: %d %d %d %d\n", stopCalcs.m128_f32[0], stopCalcs.m128_f32[1], stopCalcs.m128_f32[2], stopCalcs.m128_f32[3]);
						//printf("SIMDi stopCalcs: %d %d %d %d\n", stopCalcs.m128_f32[0], stopCalcs.m128_f32[1], stopCalcs.m128_f32[2], stopCalcs.m128_f32[3]);

						/*for (unsigned int i = 0; i < SIMD_WIDTH; i++)
						{
							printf("%3d", stopCalcsi.m128i_u32[i]);
						}
						printf("\n");*/

						//TODO: STEP 2: replace these scalar accesses with a use of _mm_test_all_ones or _mm_test_all_zeros
						//TODO: STEP 1: replace these accesses of stopCalcs to appropriate uses of the SIMD vector union
						//if (stopCalcs[0] && stopCalcs[1] && stopCalcs[2] && stopCalcs[3]) break;
						if (_mm_test_all_ones(stopCalcsi)) break;

						iterCounts = _mm_add_epi32(iterCounts, _mm_set1_epi32(1));

						//TODO: STEP 3: convert this loop to use only SIMD instructions
						for (unsigned int simdx = 0; simdx < SIMD_WIDTH; simdx++)
						{
							//TODO: STEP 3: convert this if statement to be a calculation with a selection
							//TODO: STEP 1: replace this access of stopCalcs to an appropriate use of the SIMD vector union
							if (!stopCalcs.m128_f32[simdx])
							{
								//TODO: STEP 3: convert these calculation statements to SIMD
								float xtemp = xs.m128_f32[simdx] * xs.m128_f32[simdx] - ys.m128_f32[simdx] * ys.m128_f32[simdx] + x0s.m128_f32[simdx];

								ys.m128_f32[simdx] = 2 * xs.m128_f32[simdx] * ys.m128_f32[simdx] + y0s.m128_f32[simdx];
								xs.m128_f32[simdx] = xtemp;
								iters.m128i_u32[simdx] = iterCounts.m128i_u32[simdx];
							}
						}
					}

					// if point has escaped add the number of iterations it took to do so to the running total (i.e. stable points don't add to the total)
					__m128i itersGtIterations = _mm_cmpgt_epi32(iters, _mm_set1_epi32(iterations));
					__m128i itersGtIterationsRHS = _mm_or_si128(_mm_andnot_si128(itersGtIterations, iters), _mm_and_si128(itersGtIterations, _mm_set1_epi32(0)));
					totalCalcs = _mm_add_epi32(totalCalcs, itersGtIterationsRHS);

					for (unsigned int simdx = 0; simdx < SIMD_WIDTH; simdx++)
					{
						printf("%3d", totalCalcs.m128i_u32[simdx]);
					}
					printf("\n");
				}
			}

			// convert number of iterations to colour and store in buffer
			for (unsigned int simdx = 0; simdx < SIMD_WIDTH; simdx++)
			{
				out[iy * width + ix + simdx] = iterations2colour(totalCalcs.m128i_i32[simdx] / (samples * samples), iterations, threadID % 7 + 1);
			}
		}
	}
}
