#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

// size of the buffer
const unsigned int BUFFER_SIZE = 1000000;

// buffer
float buffer[BUFFER_SIZE];

int main(int argc, char **argv)
{
	// set a random seed because
	srand(17372);

	// intialise the buffer with some random stuff and output some elements
	for (int i = 0; i < BUFFER_SIZE; i++) buffer[i] = rand() / 1000.0f;
	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", buffer[i]);
	printf("\n\n");


	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", buffer[i] * 1.75f);
	printf("\n");

	//comment out these loops
	// perform a trivial calculation and output some array elements
	//for (int i = 0; i < BUFFER_SIZE; i++) buffer[i] *= 1.75f;
	//for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", buffer[i]);
	//printf("\n");

	//all the OpenCL things
	cl_int err;

	cl_platform_id platform;

	err = clGetPlatformIDs(1, &platform, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetPlatformIDs. Error code: %d\n", err);
		exit(1);
	}

	cl_device_id deviceId;

	cl_uint numDevices = 1;

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, &deviceId, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetDeviceIDs. Error code: %d\n", err);
		exit(1);
	}

	cl_context context;

	context = clCreateContext(NULL, numDevices, &deviceId, NULL, NULL, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateContext. Error code: %d\n", err);
		exit(1);
	}

	cl_command_queue commandQueue;

	//commandQueue = clCreateCommandQueueWithProperties(context, deviceId, NULL, &err);
	commandQueue = clCreateCommandQueue(context, deviceId, NULL, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateCommandQueue. Error code: %d\n", err);
		exit(1);
	}

	const char* program =
		"__kernel void func(__global float* array)"
		"{"
		"   int i = get_global_id(0);"
		"	array[i] *= 1.75f;"
		"}";

	cl_program clProgram;

	clProgram = clCreateProgramWithSource(context, numDevices, &program, NULL, &err);
	if (err != CL_SUCCESS)
	{		
		printf("\nError calling clCreateProgramWithSource. Error code: %d\n", err);
		exit(1);
	}

	err = clBuildProgram(clProgram, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		char* program_log;
		size_t log_size;

		clGetProgramBuildInfo(clProgram, deviceId, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		program_log = (char*)malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(clProgram, deviceId, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
		printf("%s\n", program_log);
		free(program_log);
		
		//printf("\nError calling clBuildProgram. Error code: %d\n", err);
		exit(1);
	}

	cl_kernel clKernal;

	clKernal = clCreateKernel(clProgram, "func", &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateKernel. Error code: %d\n", err);
		exit(1);
	}

	cl_mem clBuffer;

	clBuffer = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, sizeof(buffer), buffer, &err);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clCreateBuffer. Error code: %d\n", err);
		exit(1);
	}

	err = clSetKernelArg(clKernal, 0, sizeof(clBuffer), &clBuffer);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clSetKernelArg. Error code: %d\n", err);
		exit(1);
	}

	const size_t workOffset = 0;
	const size_t workSize = BUFFER_SIZE;

	err = clEnqueueNDRangeKernel(commandQueue, clKernal, 1, &workOffset, &workSize, NULL, 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clEnqueueNDRangeKernel. Error code: %d\n", err);
		exit(1);
	}

	err = clEnqueueReadBuffer(commandQueue, clBuffer, true, workOffset, sizeof(buffer), buffer, NULL, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clEnqueueReadBuffer. Error code: %d\n", err);
		exit(1);
	}

	for (int i = 0; i < BUFFER_SIZE; i += BUFFER_SIZE / 7) printf("%10f", buffer[i]);
	printf("\n");

	clReleaseMemObject(clBuffer);
	clReleaseCommandQueue(commandQueue);
	clReleaseProgram(clProgram);
	clReleaseKernel(clKernal);
	clReleaseContext(context);
}
