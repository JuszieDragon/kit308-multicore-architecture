// Tutorial05_BasicSIMD.cpp 

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <immintrin.h>

void init()
{
	int array[40];
	int value = 4;

	for (int i = 0; i < 40; i++)
	{
		array[i] = value;
	}

	printf("zero:\n%d %d %d %d %d %d %d %d\n\n", array[0], array[5], array[10], array[15], array[20], array[25], array[30], array[35]);
}

void initSIMD()
{
	__m128i array[10];
	int value = 5;

	for (int i = 0; i < 10; i++)
	{
		array[i] = _mm_set1_epi32(value);
	}

	printf("zeroSIMD:\n%d %d %d %d %d %d %d %d\n\n", array[0].m128i_i32[0], array[1].m128i_i32[1], array[2].m128i_i32[2], array[3].m128i_i32[3], array[5].m128i_i32[0], array[6].m128i_i32[1], array[7].m128i_i32[2], array[8].m128i_i32[3]);
}

void printSingleSIMD(char* name, __m128i value)
{
	printf("%14s:%d %d %d %d\n", name, value.m128i_i32[0], value.m128i_i32[1], value.m128i_i32[2], value.m128i_i32[3]);
}

inline __m128i select(__m128i cond, __m128i trueVal, __m128i falseVal)
{
	return _mm_or_si128(_mm_and_si128(cond, trueVal), _mm_andnot_si128(cond, falseVal));
}

void branches1()
{
	int array[4] = { 4, 7, -2, 9 };
	int value = 5, adjustment1 = 10, adjustment2 = 3;

	for (int i = 0; i < 4; i++)
	{
		if (array[i] > value)
			array[i] += adjustment1;
		else
			array[i] += adjustment2;
	}

	printf("branches1:\n%d %d %d %d\n\n", array[0], array[1], array[2], array[3]);
}

void branches1SIMD()
{
	__m128i array = _mm_setr_epi32(4, 7, -2, 9);
	__m128i value = _mm_set1_epi32(5);

	__m128i add = select(_mm_cmpgt_epi32(array, value), _mm_set1_epi32(10), _mm_set1_epi32(3));
	array = _mm_add_epi32(array, add);

	printf("branches1SIMD:\n%d %d %d %d\n\n", array.m128i_i32[0], array.m128i_i32[1], array.m128i_i32[2], array.m128i_i32[3]);
}

void printSingleSIMD(char* name, __m256i value)
{
	printf("%14s:%d %d %d %d %d %d %d %d\n", name, value.m256i_i32[0], value.m256i_i32[1], value.m256i_i32[2], value.m256i_i32[3], value.m256i_i32[4], value.m256i_i32[5], value.m256i_i32[6], value.m256i_i32[7]);
}

inline __m256i select(__m256i cond, __m256i trueVal, __m256i falseVal)
{
	return _mm256_or_si256(_mm256_and_si256(cond, trueVal), _mm256_andnot_si256(cond, falseVal));
}

void branches2()
{
	int array[8] = { 4, -2, 9, 7, 3, 2, 4, 6 };
	int value = 5, adjustment = 3;

	for (int i = 0; i < 8; i++)
	{
		if (array[i] <= value)
			array[i] += adjustment;
	}

	printf("branches2:\n%d %d %d %d %d %d %d %d\n\n", array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);
}

void branches2SIMD()
{
	printf("branches2SIMD:\n");
	__m256i array = _mm256_setr_epi32(4, -2, 9, 7, 3, 2, 4, 6);
	__m256i value = _mm256_set1_epi32(5), adjustment = _mm256_set1_epi32(3);

	__m256i add = select(_mm256_cmpgt_epi32(value, array), adjustment, _mm256_set1_epi32(0));
	array = _mm256_add_epi32(array, add);

	//printSingleSIMD("array", array);
	printf("%d %d %d %d %d %d %d %d\n\n", array.m256i_i32[0], array.m256i_i32[1], array.m256i_i32[2], array.m256i_i32[3], array.m256i_i32[4], array.m256i_i32[5], array.m256i_i32[6], array.m256i_i32[7]);
}

void branches3()
{
	float a[8] = { 1, 2, 7, -3, 4, 2, 6, -2 };
	float b[8] = { 5, 1, 3, -3, 5, 2, 5, 1 };

	for (int i = 0; i < 8; i++)
	{
		if (a[i] <= b[i])
		{
			a[i] = b[i];
			b[i] = 0;
		}
	}

	printf("branches3:\n%f %f %f %f %f %f %f %f\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
	printf("%f %f %f %f %f %f %f %f\n\n", b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]);
}

void printSingleSIMD(char* name, __m256 value)
{
	printf("%14s:%f %f %f %f %f %f %f %f\n", name, value.m256_f32[0], value.m256_f32[1], value.m256_f32[2], value.m256_f32[3], value.m256_f32[4], value.m256_f32[5], value.m256_f32[6], value.m256_f32[7]);
}

inline __m256 select(__m256 cond, __m256 trueVal, __m256 falseVal)
{
	return _mm256_or_ps(_mm256_and_ps(cond, trueVal), _mm256_andnot_ps(cond, falseVal));
}

void branches3SIMD()
{
	//float a[8] = { 1, 2, 7, -3, 4, 2, 6, -2 };
	//float b[8] = { 5, 1, 3, -3, 5, 2, 5, 1 };
	__m256 a = _mm256_setr_ps(1, 2, 7, -3, 4, 2, 6, -2);
	__m256 b = _mm256_setr_ps(5, 1, 3, -3, 5, 2, 5, 1);

	__m256 cond = _mm256_cmp_ps(a, b, _CMP_LE_OQ);

	printf("branches3SIMD:\n");
	printSingleSIMD("a before", a);
	printSingleSIMD("b before", b);
	
	a = select(cond, b, a);
	b = select(cond, _mm256_setzero_ps(), b);

	printSingleSIMD("cond", cond);
	
	/*for (int i = 0; i < 8; i++)
	{
		a.m256_f32[i] = (a.m256_f32[i] <= b.m256_f32[i]) ? b.m256_f32[i] : a.m256_f32[i];
		b.m256_f32[i] = (a.m256_f32[i] <= b.m256_f32[i]) ? 0 : b.m256_f32[i];
	}*/

	//printf("branches3SIMD:\n%f %f %f %f %f %f %f %f\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
	//printf("%f %f %f %f %f %f %f %f\n\n", b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]);
	printf("%f %f %f %f %f %f %f %f\n", a.m256_f32[0], a.m256_f32[1], a.m256_f32[2], a.m256_f32[3], a.m256_f32[4], a.m256_f32[5], a.m256_f32[6], a.m256_f32[7]);
	printf("%f %f %f %f %f %f %f %f\n\n", b.m256_f32[0], b.m256_f32[1], b.m256_f32[2], b.m256_f32[3], b.m256_f32[4], b.m256_f32[5], b.m256_f32[6], b.m256_f32[7]);
}

void branches4()
{
	float a[8] = { 1, 2, 7, -3, 4, 2, 6, -2 };
	float b[8] = { 5, 1, 3, -3, 5, 2, 5, 1 };
	float c[8] = { 9, 8, 7, 6, 5, 4, 3, 2 };
	float e[8];

	for (int i = 0; i < 8; i++)
		if (a[i] <= b[i])
			if (a[i] == b[i])
				e[i] = a[i] * b[i] - c[i];
			else
				e[i] = c[i] + a[i];
		else
			e[i] = b[i];

	printf("branches4:\n%f %f %f %f %f %f %f %f\n\n", e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7]);
}

void branches4SIMD()
{
	__m256 a = _mm256_setr_ps(1, 2, 7, -3, 4, 2, 6, -2);
	__m256 b = _mm256_setr_ps(5, 1, 3, -3, 5, 2, 5, 1);
	__m256 c = _mm256_setr_ps(9, 8, 7, 6, 5, 4, 3, 2);
	__m256 e;

	printf("branches4SIMD:\n");
	printSingleSIMD("a", a);
	printSingleSIMD("b", b);
	printSingleSIMD("c", c);

	__m256 cond1 = _mm256_cmp_ps(a, b, _CMP_EQ_OQ);
	__m256 true1 = _mm256_sub_ps(_mm256_mul_ps(a, b), c);
	__m256 false1 = _mm256_add_ps(c, a);
	
	__m256 cond2 = _mm256_cmp_ps(a, b, _CMP_LE_OQ);
	__m256 true2 = select(cond1, true1, false1);
	__m256 false2 = b;

	e = select(cond2, true2, false2);

	printSingleSIMD("cond1", cond1);
	printSingleSIMD("true1", true1);
	printSingleSIMD("false1", false1);

	printSingleSIMD("cond2", cond2);
	printSingleSIMD("true2", true2);
	printSingleSIMD("false2", false2);
	printf("%f %f %f %f %f %f %f %f\n\n", e.m256_f32[0], e.m256_f32[1], e.m256_f32[2], e.m256_f32[3], e.m256_f32[4], e.m256_f32[5], e.m256_f32[6], e.m256_f32[7]);
}

void branches5()
{
	float a[8] = { 1, 2, 7, -3, 4, 2, 6, -2 };
	float b[8] = { 5, 1, 3, -3, 5, 2, 5, 1 };
	float c[8] = { 9, 8, 7, 6, 5, 4, 3, 2 };

	for (int i = 0; i < 8; i++)
	{
		if (a[i] == b[i]) continue;

		if (a[i] < c[i])
		{
			b[i] = a[i] + c[i];
		}
		else if (b[i] < c[i])
		{
			a[i] = a[i] * 0.1f;
			b[i] = a[i];
		}
	}
	printf("branches5:\n%f %f %f %f %f %f %f %f\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
	printf("%f %f %f %f %f %f %f %f\n\n", b[0], b[1], b[2], b[3], b[4], b[5], b[6], b[7]);
}

void branches5SIMD()
{
	__m256 a = _mm256_setr_ps(1, 2, 7, -3, 4, 2, 6, -2);
	__m256 b = _mm256_setr_ps(5, 1, 3, -3, 5, 2, 5, 1);
	__m256 c = _mm256_setr_ps(9, 8, 7, 6, 5, 4, 3, 2);

	for (int i = 0; i < 8; i++)
	{
		if (a.m256_f32[i] != b.m256_f32[i])
		{
			if (a.m256_f32[i] < c.m256_f32[i])
			{
				b.m256_f32[i] = a.m256_f32[i] + c.m256_f32[i];
			}
			else
			{
				b.m256_f32[i] = b.m256_f32[i];
			}
			
			if (a.m256_f32[i] >= c.m256_f32[i] && b.m256_f32[i] < c.m256_f32[i])
			{
				a.m256_f32[i] = a.m256_f32[i] * 0.1f;
				b.m256_f32[i] = a.m256_f32[i];
			}
			else
			{
				a.m256_f32[i] = a.m256_f32[i];
				b.m256_f32[i] = b.m256_f32[i];
			}
		}
		else
		{
			a.m256_f32[i] = a.m256_f32[i];
			b.m256_f32[i] = b.m256_f32[i];
		}
	}

	printf("branches5SIMD:\n");
	printf("%f %f %f %f %f %f %f %f\n", a.m256_f32[0], a.m256_f32[1], a.m256_f32[2], a.m256_f32[3], a.m256_f32[4], a.m256_f32[5], a.m256_f32[6], a.m256_f32[7]);
	printf("%f %f %f %f %f %f %f %f\n", b.m256_f32[0], b.m256_f32[1], b.m256_f32[2], b.m256_f32[3], b.m256_f32[4], b.m256_f32[5], b.m256_f32[6], b.m256_f32[7]);
	printf("%f %f %f %f %f %f %f %f\n\n", c.m256_f32[0], c.m256_f32[1], c.m256_f32[2], c.m256_f32[3], c.m256_f32[4], c.m256_f32[5], c.m256_f32[6], c.m256_f32[7]);
}

void horiz()
{
	float sum = 0;
	float a[80] = {
		1, 6, 5, 4, 3, 9, 8, 4,
		2.1f, 4.6f, 2.1f, 5.4f, 5.66f, 7.3f, 1.2f, 1.6f,
		1, 2, 7, -3, 4, 2, 6, -2,
		5, 1, 3, -3, 5, 2, 5, 1,
		9, 8, 7, 6, 5, 4, 3, 2,
		100, 100, 100, 100, 50, 50, 50, 50,
		4, 3, 2, 1, 0, -1, -2, -3,
		9, 6, 1, 9, 4, 3, 2, 1 
	};

	for (int i = 0; i < 64; i++)
	{
		sum += a[i];
	}

	printf("horiz:\n%f\n\n", sum);
}

void horizSIMD()
{
	float sum = 0;
	float a[80] = {
		1, 6, 5, 4, 3, 9, 8, 4,
		2.1f, 4.6f, 2.1f, 5.4f, 5.66f, 7.3f, 1.2f, 1.6f,
		1, 2, 7, -3, 4, 2, 6, -2,
		5, 1, 3, -3, 5, 2, 5, 1,
		9, 8, 7, 6, 5, 4, 3, 2,
		100, 100, 100, 100, 50, 50, 50, 50,
		4, 3, 2, 1, 0, -1, -2, -3,
		9, 6, 1, 9, 4, 3, 2, 1
	};

	for (int i = 0; i < 64; i++)
	{
		sum += a[i];
	}

	printf("horizSIMD:\n%f\n\n", sum);
}

int main(int argc, char **argv)
{
	init();
	initSIMD();

	branches1();
	branches1SIMD();
	branches2();
	branches2SIMD();
	branches3();
	branches3SIMD();
	branches4();
	branches4SIMD();
	branches5();
	branches5SIMD();

	horiz();
	horizSIMD();

	// success!
	return 0;
}

