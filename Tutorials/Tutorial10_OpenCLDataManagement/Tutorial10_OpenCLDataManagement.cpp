#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>
#include "LoadCL.h"

struct Pair
{
	cl_ushort littleNum;
	cl_int bigNum;
};

struct InnerData
{
	cl_char c;
	cl_int x;
	Pair* pairs;
	cl_char d;
};

struct ComplexData
{
	cl_int* values;
	InnerData* inner;
};

int main(int argc, char **argv)
{
	// set a random seed because
	srand(17372);

	ComplexData data;

	// intialise the data structure with some random stuff and output some elements
	data.values = new int[100];
	data.inner = new InnerData[4];

	for (int i = 0; i < 100; i++) data.values[i] = rand();
	for (int i = 0; i < 4; i++)
	{
		data.inner[i].c = rand() % 26 + 65;
		data.inner[i].x = rand();
		data.inner[i].d = rand() % 26 + 96;
		data.inner[i].pairs = new Pair[50];

		for (int j = 0; j < 50; j++)
		{
			data.inner[i].pairs[j].littleNum = rand() % 65536;
			data.inner[i].pairs[j].bigNum = rand();
		}
	}

	printf("---------------\nCPU-side data\n");
	int sum = 0;
	for (int i = 0; i < 100; i++) sum += data.values[i];
	printf("%d, %d, ... %d, %d :: %d\n", data.values[0], data.values[1], data.values[98], data.values[99], sum);
	for (int i = 0; i < 4; i++)
	{
		printf("\ninner %d: %c, %d, %c\n", i, data.inner[i].c, data.inner[i].x, data.inner[i].d);

		sum = 0;
		for (int j = 0; j < 50; j++)
		{
			sum += data.inner[i].pairs[j].littleNum;
		}
		printf("%d, %d, ... %d, %d :: %d\n", data.inner[i].pairs[0].littleNum, data.inner[i].pairs[1].littleNum, 
			data.inner[i].pairs[48].littleNum, data.inner[i].pairs[49].littleNum, sum);

		sum = 0;
		for (int j = 0; j < 50; j++)
		{
			sum += data.inner[i].pairs[j].bigNum;
		}
		printf("%d, %d, ... %d, %d :: %d\n", data.inner[i].pairs[0].bigNum, data.inner[i].pairs[1].bigNum,
			data.inner[i].pairs[48].bigNum, data.inner[i].pairs[49].bigNum, sum);

	}

	cl_int err;
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;
	cl_command_queue queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem clBufferPairs0, clBufferPairs1, clBufferPairs2, clBufferPairs3;
	cl_mem clBufferInner, clBufferValues, clBufferComplexData;
	size_t workOffset[] = { 0 };
	size_t workSize[] = { 1 };

	err = clGetPlatformIDs(1, &platform, NULL);
	if (err != CL_SUCCESS)
	{
		printf("\nError calling clGetPlatformIDs. Error code: %d\n", err);
		exit(1);
	}

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	if (err != CL_SUCCESS) {
		printf("Couldn't find any devices\n");
		exit(1);
	}

	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a context\n");
		exit(1);
	}

	queue = clCreateCommandQueue(context, device, 0, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create the command queue\n");
		exit(1);
	}

	program = clLoadSource(context, "OutputData.cl", &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't load/create the program\n");
		exit(1);
	}

	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS) {
		char *program_log;
		size_t log_size;

		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		program_log = (char*)malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
		printf("%s\n", program_log);
		free(program_log);
		exit(1);
	}

	kernel = clCreateKernel(program, "func", &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create the kernel\n");
		exit(1);
	}

	// will need multiple buffers
	clBufferPairs0 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Pair) * 50, &data.inner[0].pairs[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}
	
	clBufferPairs1 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Pair) * 50, &data.inner[1].pairs[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}
	
	clBufferPairs2 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Pair) * 50, &data.inner[2].pairs[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}
	
	clBufferPairs3 = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Pair) * 50, &data.inner[3].pairs[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	clBufferInner = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(InnerData) * 4, &data.inner[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	clBufferValues = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * 100, &data.values[0], &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	clBufferComplexData = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(ComplexData), &data, &err);
	if (err != CL_SUCCESS) {
		printf("Couldn't create a bufferIn1 object\n");
		exit(1);
	}

	// will need to pass all the buffers through
	err = clSetKernelArg(kernel, 0, sizeof(clBufferPairs0), &clBufferPairs0);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clSetKernelArg(kernel, 1, sizeof(clBufferPairs1), &clBufferPairs1);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}
	err = clSetKernelArg(kernel, 2, sizeof(clBufferPairs2), &clBufferPairs2);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clSetKernelArg(kernel, 3, sizeof(clBufferPairs3), &clBufferPairs3);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clSetKernelArg(kernel, 4, sizeof(clBufferInner), &clBufferInner);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clSetKernelArg(kernel, 5, sizeof(clBufferValues), &clBufferValues);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clSetKernelArg(kernel, 6, sizeof(clBufferComplexData), &clBufferComplexData);
	if (err != CL_SUCCESS) {
		printf("Couldn't set the kernel argument\n");
		exit(1);
	}

	err = clEnqueueNDRangeKernel(queue, kernel, 1, workOffset, workSize, NULL, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printf("Couldn't enqueue the kernel execution command\n");
		exit(1);
	}

	// cleanup buffers
	clReleaseMemObject(clBufferPairs0);
	clReleaseMemObject(clBufferPairs1);
	clReleaseMemObject(clBufferPairs2);
	clReleaseMemObject(clBufferPairs3);
	clReleaseMemObject(clBufferInner);
	clReleaseMemObject(clBufferValues);
	clReleaseMemObject(clBufferComplexData);
	clReleaseCommandQueue(queue);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseContext(context);
}
