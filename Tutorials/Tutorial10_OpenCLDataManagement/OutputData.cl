﻿//make appropriate datastructures
typedef struct Pair
{
	unsigned short littleNum;
	int bigNum;
} Pair;

typedef struct InnerData
{
	char c;
	int x;
	__global Pair* pairs;
	char d;
} InnerData;

typedef struct ComplexData
{
	__global int* values;
	__global InnerData* inner;
} ComplexData;

// add an appropriate set of parameters to transfer the data
__kernel void func(__global Pair* pair0, __global Pair* pair1, __global Pair* pair2, __global Pair* pair3, __global InnerData* inner, __global int* values, __global ComplexData* data) {
	unsigned int i = get_global_id(0); 

	// relink the pointers
	data->values = values;
	data->inner = inner;
	data->inner[0].pairs = pair0;
	data->inner[1].pairs = pair1;
	data->inner[2].pairs = pair2;
	data->inner[3].pairs = pair3;

	if (i == 0)
	{
		printf("\n---------------\nGPU-side data\n");

		// print out the data
		int sum = 0;
		for (int i = 0; i < 100; i++) sum += data->values[i];
		printf("%d, %d, ... %d, %d :: %d\n", data->values[0], data->values[1], data->values[98], data->values[99], sum);
		for (int i = 0; i < 4; i++)
		{
			printf("\ninner %d: %c, %d, %c\n", i, data->inner[i].c, data->inner[i].x, data->inner[i].d);

			sum = 0;
			for (int j = 0; j < 50; j++)
			{
				sum += data->inner[i].pairs[j].littleNum;
			}
			printf("%d, %d, ... %d, %d :: %d\n", data->inner[i].pairs[0].littleNum, data->inner[i].pairs[1].littleNum,
				data->inner[i].pairs[48].littleNum, data->inner[i].pairs[49].littleNum, sum);

			sum = 0;
			for (int j = 0; j < 50; j++)
			{
				sum += data->inner[i].pairs[j].bigNum;
			}
			printf("%d, %d, ... %d, %d :: %d\n", data->inner[i].pairs[0].bigNum, data->inner[i].pairs[1].bigNum,
				data->inner[i].pairs[48].bigNum, data->inner[i].pairs[49].bigNum, sum);

		}
	}
}