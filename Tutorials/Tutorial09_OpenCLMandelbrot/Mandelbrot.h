#ifndef _MANDELBROT_H
#define _MANDELBROT_H

// Mandelbrot defaults and limits
#define MAX_WIDTH 2084
#define MAX_HEIGHT 2084
#define MAX_COLOURS 256				// must be a power of 2
#define DEFAULT_WIDTH 1024		// no bigger than MAX_WIDTH
#define DEFAULT_HEIGHT 1024			// no bigger than MAX_HEIGHT

#define DEFAULT_MAX_ITER 2048
#define DEFAULT_CENTRE_X -0.5275f;
#define DEFAULT_CENTRE_Y 0.5025f;
#define DEFAULT_SCALE_Y 0.005f;

#define DEFAULT_SAMPLES 128

#endif
